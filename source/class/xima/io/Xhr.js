/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Class.define('xima.io.Xhr', {
	 extend: qx.io.request.Xhr,

	/**
	 * @param url {String?} La URL para la petición del recurso.
	 * @param method {String?} El método HTTP a utilizar.
	 */
	construct: function(url, method) {
		var app = qx.core.Init.getApplication();
		url = app.getApiUrl() + url;
		this.base(arguments, url, method);
		this.setAccept('application/json');
	 }
});
