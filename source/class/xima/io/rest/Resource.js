/* ****************************************************************

 @link https://bitbucket.org/xibalba/xima
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Xima: Mezcal CMS client application --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Resource class
 * This class provide a construction with a custom base URL by default.
 */
qx.Class.define('xima.io.rest.Resource', {
	extend: qx.io.rest.Resource,

	construct: function(description) {
		this.base(arguments, description);
		this.setBaseUrl(qx.core.Init.getApplication().getApiUrl());
	},

	members: {
		_getRequest: function() {
			return new xima.io.Xhr();
		}
	}
});
