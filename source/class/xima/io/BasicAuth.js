/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 *
 * Class BasicAuth
 * To void send the password string as a simple plain text,
 * this class provide a wrapper that encode as a `SHA-256` hash
 * the actual password value.
 *
 * A instance of this class must be setted to a Request by the «setAuthentication()» method.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @asset(vendor/js/sha256.js)
 */
qx.Class.define('xima.io.BasicAuth', {
    extend: qx.io.request.authentication.Basic,

    /**
     * @param username {String} User name.
     * @param password {String} Password.
     */
    construct: function(username, password) {
        var hasher = new jsSHA(username + password, "TEXT");
        this.__credentials = qx.util.Base64.encode(username + ':' + hasher.getHash("SHA-256", "HEX"));
    }
});