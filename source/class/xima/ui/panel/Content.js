/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/address-book-new.svg)
 */
qx.Class.define('xima.ui.panel.Content', {
	extend: piche.ui.panel.Basic,

	construct: function() {
		this.base(arguments, 'Content', 'xima/icons/news-subscribe.svg');
		this.setLayout(new qx.ui.layout.Grow());

		this.add(this.getTabView());
	},

	members: {
		__tabView: null,
		__dasboardTab: null,
		__splitPane: null,
		__taxoList: null,
		__contentList: null,
		__toolBarL: null,
		__toolBarC: null,
		__taxoWinEditor: null,

		getTaxoWinEditor: function() {
			if(this.__taxoWinEditor) return this.__taxoWinEditor;

			this.__taxoWinEditor = new xima.ui.window.Editor('Edit a taxo element', null, new xima.ui.form.Taxo());
			this.__taxoWinEditor.addListener("close", function(e) {
				e.getTarget().dispose();
				this.__taxoWinEditor = null;
			}, this);

			return this.__taxoWinEditor;
		},

		getDashboardTab: function() {
			if(this.__dasboardTab) return this.__dasboardTab;

			this.__dasboardTab = new qx.ui.tabview.Page('Dashboard', 'xima/icons/address-book-new.svg');
			this.__dasboardTab.setLayout(new qx.ui.layout.Grow());

			this.__dasboardTab.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});

			var splitPane = this.getSplitPane();

			var compoLeft = new qx.ui.container.Composite(new qx.ui.layout.VBox());
			var compoRight = new qx.ui.container.Composite(new qx.ui.layout.VBox());

			compoLeft.add(this.getToolBar('taxo'));
			compoLeft.add(this.getTaxoList(), {flex: 1});

			compoRight.add(this.getToolBar('content'));
			compoRight.add(this.getContentList(), {flex: 1});

			splitPane.add(compoLeft, 1);
			splitPane.add(compoRight, 3);

			this.__dasboardTab.add(splitPane);

			return this.__dasboardTab;
		},

		getTabView: function() {
			if(this.__tabView) return this.__tabView;

			this.__tabView = new qx.ui.tabview.TabView();
			this.__tabView.add(this.getDashboardTab());
			return this.__tabView;
		},

		getSplitPane: function () {
			if(this.__splitPane) return this.__splitPane;

			this.__splitPane = new qx.ui.splitpane.Pane();
			return this.__splitPane;
		},

		getTaxoList: function() {
			if(this.__taxoList) return this.__taxoList;

			this.__taxoList = new qx.ui.list.List().set({
				labelPath: "label"
			});
			return this.__taxoList;
		},

		getContentList: function() {
			if(this.__contentList) return this.__contentList;

			this.__contentList = new qx.ui.list.List();
			return this.__contentList;
		},

		/**
		 * Return the toolbar instance of compose one if do not extist.
		 * @returns {qx.ui.toolbar.ToolBar}
		 */
		getToolBar: function(id) {
			if(id == 'taxo') {
				if(this.__toolBarL) return this.__toolBarL;

				this.__toolBarL = new xima.ui.toolbar.Ned();
				return this.__toolBarL;
			}
			else {
				if(this.__toolBarC) return this.__toolBarC;

				this.__toolBarC = new xima.ui.toolbar.Ned();
				return this.__toolBarC;
			}
		}
	}
});
