/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/configure.svg)
 * @asset(xima/icons/quickopen-class.svg)
 */
qx.Class.define("xima.ui.panel.ContentProperties", {
	extend: piche.ui.panel.Basic,

	construct: function() {
		this.base(arguments, 'Properties', 'xima/icons/configure.svg');

		this.setLayout(new qx.ui.layout.VBox(3));
		this.getChildControl('icon').set({width: 22, height: 22});
		this.getChildrenContainer().setPaddingLeft(10);
		this.getChildrenContainer().setPaddingTop(5);

		this.__atoms = {};

		this.add(this.__getAtom("taxo"));
		this.add(this.__getAtom("status"));
		this.add(this.__getAtom("slug"));
		this.add(this.__getAtom("author"));
		this.add(this.__getAtom("editor"));
		this.add(this.__getAtom("createdAt"));
		this.add(this.__getAtom("updatedAt"));
		this.add(this.__getAtom("publishedAt"));
	},

	properties: {
		taxo: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		status: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		slug: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		author: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		editor: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		createdAt: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		updatedAt: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		publishedAt: {
			check: "String",
			apply: "__applyPublishedAt",
			nullable: true
		}
	},

	members: {
		__atoms: null,

		__applyAtomLabel: function(value, old, propertyName) {
			this.__getAtom(propertyName).setLabel(value);
		},

		// Since is possible set a null value, then a «Not yet publishied» must be setted on null.
		__applyPublishedAt: function(value, old, propertyName) {
			if(value) this.__getAtom(propertyName).setLabel(value);
			else this.__getAtom(propertyName).setLabel('Not yet publishied');
		},

		/**
		 * Retrive an Atom instance for any supported `id` to be shown as a content property data.
		 * @param id {String}
		 * @return {qx.ui.basic.Atom}
		 * @private
		 */
		__getAtom: function(id) {
			if(this.__atoms[id]) return this.__atoms[id];

			switch (id) {
				case "taxo":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/tag.svg');
					this.__atoms[id].setToolTipText("Content taxonomy.");
					break;
				case "status":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/quickopen-class.svg');
					this.__atoms[id].setToolTipText("Content Status.");
					break;
				case "slug":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/emblem-symbolic-link.svg');
					this.__atoms[id].setToolTipText("Content slug.");

					break;
				case "author":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/im-user.svg');
					this.__atoms[id].setToolTipText("Content author username.");

					break;
				case "editor":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/im-user-away.svg');
					this.__atoms[id].setToolTipText("Username of content editor (last user on edit the content).");

					break;
				case "createdAt":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/view-calendar-special-occasion.svg');
					this.__atoms[id].setToolTipText("Content creation date.");

					break;
				case "updatedAt":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/view-calendar-upcoming-events.svg');
					this.__atoms[id].setToolTipText("Content last edition date.");

					break;
				case "publishedAt":
					this.__atoms[id] = new qx.ui.basic.Atom("Not yet publishied", 'xima/icons/view-calendar-upcoming-days.svg');
					this.__atoms[id].setToolTipText("Content publication date.");

					break;
				default:
					throw new Error("Unsupported atom id.");
			}

			this.__atoms[id].getChildControl('icon').set({width: 22, height: 22});
			return this.__atoms[id];
		},

		/**
		 * Clean all internal atom widgets.
		 */
		cleanUp: function() {
			this.setAtom(null);
			this.setStatus(null);
			this.setSlug(null);
			this.setAuthor(null);
			this.setEditor(null);
			this.setCreatedAt(null);
			this.setUpdatedAt(null);
			this.setPublishedAt(null);
		}
	}
});
