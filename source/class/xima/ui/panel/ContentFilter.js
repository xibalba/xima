/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class ContentFilter
 * This class provide widgets to filter the content list to be shown to the user.
 * @asset(xima/icons/view-filter.svg)
 */
qx.Class.define("xima.ui.panel.ContentFilter", {
	extend: piche.ui.panel.Basic,

	construct: function() {
		this.base(arguments, 'Filters', 'xima/icons/view-filter.svg');

		this.setLayout(new qx.ui.layout.VBox(3));
		this.getChildControl('icon').set({width: 22, height: 22});
		this.getChildrenContainer().setPaddingLeft(5);
		this.getChildrenContainer().setPaddingRight(10);
		this.getChildrenContainer().setPaddingTop(5);
		this.setShowCloseButton(false);

		this.__selectBoxes = {};
		this.__atoms = {};

		this.add(new qx.ui.basic.Label("<b>Taxonomy:</b>").set({rich: true}));
		this.add(this.getSelectBox("taxo"));
		this.add(new qx.ui.basic.Label("<b>Status:</b>").set({rich: true}));
		this.add(this.getSelectBox("status"));

		this.add(this.getToolBar());
	},

	properties: {
		taxonomy: {
			check: "String",
			apply: "__applyTaxo",
			nullable: true
		}
	},

	members: {
		__toolbar: null,
		__selectBoxes: null,
		__atoms: null,

		/**
		 * Return the Content Filter toolbar instance.
		 * @return {xima.ui.toolbar.ContentFilter}
		 */
		getToolBar: function() {
			if(this.__toolbar) return this.__toolbar;

			this.__toolbar = new xima.ui.toolbar.ContentFilter();
			return this.__toolbar;
		},

		/**
		 *
		 * @param id {String}
		 * @return {qx.ui.form.SelectBox}
		 */
		getSelectBox: function(id) {
			if(this.__selectBoxes[id]) return this.__selectBoxes[id];

			if(id == "taxo") this.__selectBoxes[id] = new qx.ui.form.SelectBox();
			else if(id == "status") this.__selectBoxes[id] = new qx.ui.form.SelectBox();
			else throw new Error("Unsupported select box id.");

			return this.__selectBoxes[id];
		}
	}
});
