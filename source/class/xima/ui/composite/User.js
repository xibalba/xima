/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class User
 *
 * This class provice a composite component for UI that handle user data.
 */
qx.Class.define("xima.ui.composite.User", {
	extend: qx.ui.container.Composite,

	construct: function(includePasswordFields) {
		includePasswordFields = includePasswordFields || false;
		this.base(arguments, new qx.ui.layout.VBox());

		this.__boxes = {};
		this.__forms = {};

		this.add(this.getBox('user'));
		if (includePasswordFields) this.add(this.getBox('password'));
		this.add(this.getBox('person'));
	},

	members: {
		__boxes: null,
		__forms: null,

		/**
		 * Retrive an internat GroupBox component.
		 *
		 * @param id {String}
		 * @return {qx.ui.groupbox.GroupBox}
		 */
		getBox: function(id) {
			if(this.__boxes[id]) return this.__boxes[id];

			var label = '';

			if(id == 'user') label = 'User data';
			else if(id == 'password') label = 'Password';
			else if (id == 'person') label = 'Personal data';
			else throw new Error('Unsupported box id. ' + id);

			this.__boxes[id] = new qx.ui.groupbox.GroupBox(label);
			this.__boxes[id].setLayout(new qx.ui.layout.Basic());
			this.__boxes[id].add(new qx.ui.form.renderer.Single(this.getForm(id)));
			this.__boxes[id].setAllowGrowX(false);

			return this.__boxes[id];
		},

		/**
		 * Retrive a form instance of user data by the passed id.
		 *
		 * @param id {String}
		 * @return {piche.ui.form.Form}
		 */
		getForm: function(id) {
			if(this.__forms[id]) return this.__forms[id];

			if(id == 'user') this.__forms[id] = new xima.ui.form.User();
			else if(id == 'person') this.__forms[id] = new xima.ui.form.Person();
			else if(id == 'password') this.__forms[id] = new xima.ui.form.PasswordChanger();
			else throw new Error('Unsupported form id. ' + id);

			return this.__forms[id];
		}
	}
});
