/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Class.define("xima.ui.mdeditor.ToolBar", {
	extend: qx.ui.toolbar.ToolBar,

	construct: function() {
		this.base(arguments);
		this.__buttons = {};

		this.add(this.getButton("bold"));
		this.add(this.getButton("italic"));
		this.add(this.getButton("heading"));

		this.addSeparator();

		this.add(this.getButton("quote"));
		this.add(this.getButton("unordered-list"));
		this.add(this.getButton("ordered-list"));
		this.add(this.getButton("code"));

		this.addSeparator();

		this.add(this.getButton("link"));
		this.add(this.getButton("image"));
		this.add(this.getButton("table"));

		this.addSeparator();

		this.add(this.getButton("preview"));
		this.add(this.getButton("help"));
	},

	members: {
		__buttons: null,

		getButton: function (id) {
			if(this.__buttons[id]) return this.__buttons[id];

			switch (id) {
				case "bold":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createBold();
					break;

				case "italic":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createItalic();
					break;

				case "heading":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createHeading();
					break;

				case "quote":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createQuote();
					break;

				case "unordered-list":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createUnorderedList();
					break;

				case "ordered-list":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createOrderedList();
					break;

				case "code":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createCode();
					break;

				case "link":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createLink();
					break;

				case "image":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createImage();
					break;

				case "table":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createTable();
					break;

				case "preview":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createPreview();
					break;

				case "help":
					this.__buttons[id] = xima.ui.toolbar.button.Factory.createHelp();
					break;
				default:
					this.debug("Unssuported id.");
			}

			return this.__buttons[id];
		}
	}
});
