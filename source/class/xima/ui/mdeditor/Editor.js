/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/
/**
 * Class Editor
 * this class is a wrapper over the SimpleMDE Adpter widget to provide
 * a wudget with a qx toolbar and qx kind methods.
 */
qx.Class.define("xima.ui.mdeditor.Editor", {
	extend: qx.ui.core.Widget,

	construct: function() {
		this.base(arguments);
		this._setLayout(new qx.ui.layout.VBox());

		var toolbar = this.getToolBar();

		this._add(toolbar);
		this._add(this.__getMdEditor(), {flex: 1});

		// Regiter toolbar listeners

		toolbar.getButton("bold").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().toggleBold();
		}, this);

		toolbar.getButton("italic").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().toggleItalic();
		}, this);

		toolbar.getButton("heading").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().toggleHeading1();
		}, this);

		toolbar.getButton("quote").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().toggleBlockquote();
		}, this);

		toolbar.getButton("unordered-list").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().toggleUnorderedList();
		}, this);

		toolbar.getButton("ordered-list").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().toggleOrderedList();
		}, this);

		toolbar.getButton("code").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().toggleCodeBlock();
		}, this);

		toolbar.getButton("link").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().drawLink();
		}, this);

		toolbar.getButton("image").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().drawImage();
		}, this);

		toolbar.getButton("table").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().drawTable();
		}, this);

		toolbar.getButton("preview").addListener("execute", function(e) {
			this.__getMdEditor().getEditor().togglePreview();
		}, this);
	},

	members: {
		__toolbar: null,
		__mdEditor: null,

		/**
		 * Retrive the SimpleMDE Apdater instance.
		 * @return {xima.ui.mdeditor.Adapter}
		 * @private
		 */
		__getMdEditor: function() {
			if(this.__mdEditor) return this.__mdEditor;

			this.__mdEditor = new xima.ui.mdeditor.Adapter();
			return this.__mdEditor;
		},

		/**
		 * Retrive the custom xima toolbar for the editor.
		 * @return {xima.ui.mdeditor.ToolBar}
		 */
		getToolBar: function () {
			if(this.__toolbar) return this.__toolbar;

			this.__toolbar = new xima.ui.mdeditor.ToolBar();
			return this.__toolbar;
		},

		/**
		 * Retrive the editor value.
		 * @return {String}
		 */
		getValue: function() {
			return this.__getMdEditor().getEditor().value();
		},

		/**
		 * Set the editor value.
		 * @param value {String}
		 */
		setValue: function(value) {
			this.__getMdEditor().getEditor().value(value);
		}
	}
});