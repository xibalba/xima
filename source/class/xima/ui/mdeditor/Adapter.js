/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Apdarter
 * This class is a wrapper wdiget to the SimpleMDE markdown editor.
 * This class allow to deploy a bare SimpleMDE Editor into any qx container that support Widgets.
 *
 * Since this class avoid the SimpleMDE toolbar implementation, then expose an accesor method to editor instance
 * to allow external calls, for example to those methods called by the toolbar.
 */
qx.Class.define("xima.ui.mdeditor.Adapter", {
	extend: qx.ui.core.Widget,

	construct: function() {
		this.base(arguments);
		this.addListenerOnce("appear", this._appearRenderer, this);
	},

	properties: {
		// overridden
		appearance: {
			refine: true,
			init: "mdeditor"
		}
	},

	members: {
		// SimpleMDE instance reference
		__editor: null,

		// CodeMirror element reference
		__cm: null,

		// CodeMirror scrollbar reference
		__cmScroll: null,

		// SimplemDE statusbar element reference
		__cmStatusbar: null,


		/**
		 * Once the widget has appear, then exist the html elements needed to build the Editor Instance.
		 * This method process that build.
		 *
		 * @param event
		 * @protected
		 */
		_appearRenderer: function(event) {
			var contentElement = this.getContentElement();
			var domElement = contentElement.getDomElement();
			var textArea = this.getContentElement().getChild(0);

			contentElement.setStyles(
				qx.theme.manager.Font.getInstance().resolve(this.getFont()).getStyles()
			);

			this.__editor = new SimpleMDE({
				element: textArea.getDomElement(),
				spellChecker: false,
				toolbar: false
			});

			this.__cm = qx.bom.Selector.query(".CodeMirror", domElement)[0];
			this.__cmScroll = qx.bom.Selector.query(".CodeMirror-scroll", domElement)[0];
			this.__cmStatusbar = qx.bom.Selector.query(".editor-statusbar", domElement)[0];

			// Add the resize listener which will resize the editor to the size of the widet
			this.addListener("resize", this._setCodeMirrorHeight, this);
			this._setCodeMirrorHeight();
		},

		// Overriden
		_createContentElement: function () {
			// Create a div content element which will be the parent for the editor div
			var div = new qx.html.Element("div");

			// And add a textarea as a child to the former div as the editor only sets the textarea to display:none
			div.add(new qx.html.Input("textarea"));

			return div;
		},

		/**
		 * Recalc the height for inner CodeMirror element.
		 * @protected
		 */
		_setCodeMirrorHeight: function() {
			this.debug("_setCodemirrorHeight");

			if(this.__cm && this.__cmScroll) {
				var hint = this.getBounds();
				var statusbarHeight = qx.bom.element.Dimension.getHeight(this.__cmStatusbar);
				var decoHeight = qx.bom.element.Dimension.getHeight(this.__cm) - qx.bom.element.Dimension.getContentHeight(this.__cm);

				var height = ( hint.height - statusbarHeight - decoHeight) + "px";

				this.debug("_setCodemirrorHeight RECALC new Height: " + height);

				this.__cm.style.height = height;
				this.__cm.style.minHeight = height;
				this.__cmScroll.style.minHeight = height;
			}
		},

		/**
		 *
		 * @return {SimpleMDE}
		 */
		getEditor: function() {
			return this.__editor;
		},

		destruct: function() {
			this.__editor = null;
			this.__cm = null;
			this.__cmScroll = null;
			this.__cmStatusbar = null;
		}
	}
});
