/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * This UI widget compose the Front view for a design based on a TabView.
 * Over the simple TabView exist support to windows.
 */
qx.Class.define('xima.ui.Front', {
	extend: piche.ui.tabview.TabView,
	include: [ piche.mixins.BusyBlocker ],

	construct: function() {
		this.base(arguments);
		this.__windows = {};
	},

	members: {
		__windows: null,

		getWindow: function(id) {
			if(this.__windows[id]) return this.__windows[id];

			if(id == 'login') this.__windows[id] = new xima.ui.window.Login();

			return this.__windows[id];
		},

		cleanUp: function() {
			this.removeAll();
		}
	}
});