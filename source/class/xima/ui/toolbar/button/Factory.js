/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/document-edit.svg)
 * @asset(xima/icons/document-new.svg)
 * @asset(xima/icons/document-save.svg)
 * @aseet(xima/icons/database-change-key.svg)
 * @asset(xima/icons/edit-delete.svg)
 * @asset(xima/icons/format-text-bold.svg)
 * @asset(xima/icons/format-text-italic.svg)
 * @asset(xima/icons/format-text-capitalize.svg)
 * @asset(xima/icons/format-text-blockquote.svg)
 * @asset(xima/icons/format-list-unordered.svg)
 * @asset(xima/icons/format-list-ordered.svg)
 * @asset(xima/icons/format-text-code.svg)
 * @asset(xima/icons/insert-link.svg)
 * @asset(xima/icons/insert-image.svg)
 * @asset(xima/icons/insert-table.svg)
 * @asset(xima/icons/visibility.svg)
 * @asset(xima/icons/help-about.svg)
 * @asset(xima/icons/edit-find.svg)
 * @asset(xima/icons/view-filter.svg)
 */
qx.Class.define('xima.ui.toolbar.button.Factory', {
	type: 'static',

	statics: {
		create: function(label, icon) {
			var btn = new qx.ui.toolbar.Button(label, icon);
			btn.getChildControl('icon').set({width: 22, height: 22});
			return btn;
		},

		createCancel: function() {
			return this.create(null, 'xima/icons/dialog-cancel.svg');
		},

		createToggleable: function(label, icon) {
			var btn = new qx.ui.toolbar.CheckBox(label, icon);
			btn.getChildControl('icon').set({width: 22, height: 22});
			return btn;
		},

		createNew: function() {
			return this.create(null, 'xima/icons/document-new.svg');
		},

		createEdit: function() {
			return this.create(null, 'xima/icons/document-edit.svg');
		},

		createDelete: function() {
			return this.create(null, 'xima/icons/edit-delete.svg');
		},

		createSave: function() {
			return this.create(null, 'xima/icons/document-save.svg');
		},

		createBold: function() {
			return this.create(null, 'xima/icons/format-text-bold.svg');
		},

		createItalic: function() {
			return this.create(null, 'xima/icons/format-text-italic.svg');
		},

		createHeading: function() {
			return this.create(null, 'xima/icons/format-text-capitalize.svg');
		},

		createQuote: function() {
			return this.create(null, 'xima/icons/format-text-blockquote.svg');
		},

		createUnorderedList: function() {
			return this.create(null, 'xima/icons/format-list-unordered.svg');
		},

		createOrderedList: function () {
			return this.create(null, 'xima/icons/format-list-ordered.svg');
		},

		createCode: function() {
			return this.create(null, 'xima/icons/format-text-code.svg');
		},

		createChangePass: function() {
			return this.create(null, 'xima/icons/database-change-key.svg');
		},

		createLink: function () {
			return this.create(null, 'xima/icons/insert-link.svg');
		},

		createImage: function() {
			return this.create(null, 'xima/icons/insert-image.svg');
		},

		createTable: function() {
			return this.create(null, 'xima/icons/insert-table.svg');
		},

		createPreview: function() {
			return this.createToggleable(null, 'xima/icons/visibility.svg');
		},

		createHelp: function() {
			return this.create(null, 'xima/icons/help-about.svg');
		},

		createProperties: function () {
			return this.createToggleable(null, 'xima/icons/configure.svg');
		},

		createSearch: function () {
			return this.create(null, 'xima/icons/edit-find.svg');
		},

		createFilter: function() {
			return this.create(null, 'xima/icons/view-filter.svg');
		},

		/**
		 * Shorthand for factory methods.
		 * This method call to other factory methods in function of passed id.
		 *
		 * @param id {String}
		 * @return {qx.ui.form.Button}
		 */
		compose: function(id) {
			var button;

			switch (id) {
				case 'cancel':
					button = this.createCancel();
					break;
				case 'search':
					button = this.createSearch();
					break;
				case 'filter':
					button = this.createFilter();
					break;
				case 'edit':
					button = this.createEdit();
					break;
				case 'new':
					button = this.createNew();
					break;
				case 'save':
					button = this.createSave();
					break;
				case 'change-pass':
					button = this.createChangePass();
					break;
			}

			if(button) return button;
			else throw new Error(id + " is a unsupported id.");
		}
	}
});
