/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Basic
 * This class extend the qx toolbar to provide factory methods that serve of
 * toolbar button factory.
 */
qx.Class.define("xima.ui.toolbar.Basic", {
	extend: qx.ui.toolbar.ToolBar,

	construct: function(vertical) {
		vertical = vertical || false;
		this.base(arguments);
		this.__buttons = {};
	
		if(vertical) this._setLayout(new qx.ui.layout.VBox());
	},

	members: {
		__buttons: null,

		/**
		 * Return a button instance by id.
		 * @param id {String}
		 * @param autoCompose {Boolean}
		 * @return {qx.ui.toolbar.Button}
		 */
		getButton: function(id, autoCompose) {
			autoCompose = autoCompose || false;

			if(this.__buttons[id]) return this.__buttons[id];
			if(autoCompose) this.composeButton(id);

			return this.__buttons[id];
		},

		/**
		 * Compose a toolbar button and add it to the actual toolbar.
		 * @param id {String}
		 */
		composeButton: function (id) {
			if(this.__buttons[id]) throw new Error(id + " id already exist.");

			var button = xima.ui.toolbar.button.Factory.compose(id);
			if (button) {
				this.__buttons[id] = button;
				this.add(this.__buttons[id]);
			}
		}
	}
});
