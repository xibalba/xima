/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Class.define("xima.ui.toolbar.Ned", {
	extend: xima.ui.toolbar.Basic,

	members: {
		// @todo use autoCompose
		getButton: function(id) {
			if(this.__buttons[id]) return this.__buttons[id];

			if(id == 'new') this.__buttons[id] = xima.ui.toolbar.button.Factory.createNew(false);
			else if(id == 'edit') this.__buttons[id] = xima.ui.toolbar.button.Factory.createEdit(false);
			else if(id == 'delete') this.__buttons[id] = xima.ui.toolbar.button.Factory.createDelete(false);
			else throw new Error('Unssuported button id.');

			this.add(this.__buttons[id]);

			return this.__buttons[id];
		}
	}
});
