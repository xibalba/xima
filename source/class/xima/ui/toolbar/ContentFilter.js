/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Class.define("xima.ui.toolbar.ContentFilter", {
	extend: xima.ui.toolbar.Basic,

	construct: function() {
		this.base(arguments);

		this.addSpacer();
		this.composeButton('filter');
		this.addSpacer();
	}
});