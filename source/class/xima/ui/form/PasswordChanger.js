/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Password changer UI Form
 * This class provide the fields for a user (and person) data.
 */
qx.Class.define('xima.ui.form.PasswordChanger', {
	extend: piche.ui.form.Form,

	construct: function() {
		this.base(arguments);

		this.add(new qx.ui.form.PasswordField().set({
			required: true,
			width: 200,
			requiredInvalidMessage: 'Required'
		}), 'New password', null, 'prepassword');

		this.add(new qx.ui.form.PasswordField().set({
			required: true,
			width: 200,
			requiredInvalidMessage: 'Required'
		}), 'Repeat password', null, 'newpassword');
	}
});
