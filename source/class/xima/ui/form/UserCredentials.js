/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * UserCredentials UI Form
 * This class provide the fields for User and Password.
 */
qx.Class.define('xima.ui.form.UserCredentials', {
	extend: piche.ui.form.Form,

	construct: function() {
		this.base(arguments);

		this.add(new qx.ui.form.TextField().set({
			required: true,
			width: 200,
			requiredInvalidMessage: 'Required'
		}), 'User', null, 'user');

		this.add(new qx.ui.form.PasswordField().set({
			required: true,
			width: 200,
			requiredInvalidMessage: 'Required'
		}), 'Password', null, 'password');
	}
});
