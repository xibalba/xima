/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Person UI Form
 * This class provide the fields for a user (and person) data.
 */
qx.Class.define('xima.ui.form.Person', {
	extend: piche.ui.form.Form,

	construct: function() {
		this.base(arguments);

		this.add(new qx.ui.form.TextField().set({
			required: true,
			width: 300,
			requiredInvalidMessage: 'Required'
		}), 'Name', null, 'name');

		this.add(new qx.ui.form.TextField().set({
			required: true,
			width: 300,
			requiredInvalidMessage: 'Required'
		}), 'Surname', null, 'surname');
	}
});