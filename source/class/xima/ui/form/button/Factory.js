/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/document-edit.svg)
 * @asset(xima/icons/document-new.svg)
 * @asset(xima/icons/document-save.svg)
 * @asset(xima/icons/edit-delete.svg)
 * @asset(xima/icons/dialog-ok-apply.svg)
 * @asset(xima/icons/dialog-cancel.svg)
 */
qx.Class.define('xima.ui.form.button.Factory', {
	type: 'static',

	statics: {
		create: function(label, icon) {
			var btn = new qx.ui.form.Button(label, icon);
			btn.getChildControl('icon').set({width: 22, height: 22});
			return btn;
		},

		createSave: function(withLabel) {
			withLabel = withLabel || true;
			if(withLabel) withLabel = 'Save';
			return this.create(withLabel, 'xima/icons/document-save.svg');
		},

		createOk: function(withLabel) {
			withLabel = withLabel || true;
			if(withLabel) withLabel = 'Ok';
			return this.create(withLabel, 'xima/icons/dialog-ok-apply.svg');
		},

		createCancel: function(withLabel) {
			withLabel = withLabel || true;
			if(withLabel) withLabel = 'Cancel';
			return this.create(withLabel, 'xima/icons/dialog-cancel.svg');
		}
	}
});
