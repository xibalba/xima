/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Role UI Form
 * This class provide the fields for a role element.
 */
qx.Class.define('xima.ui.form.Role', {
	extend: piche.ui.form.Form,

	construct: function() {
		this.base(arguments);

		this.add(new qx.ui.form.TextField().set({
			required: true,
			width: 120,
			requiredInvalidMessage: 'Required'
		}), 'Label', null, 'label');

		this.add(new qx.ui.form.TextField().set({
			required: true,
			width: 300,
			requiredInvalidMessage: 'Required'
		}), 'Description', null, 'description');

		this.add(new qx.ui.form.TextField().set({
			required: true,
			width: 300,
			requiredInvalidMessage: 'Required'
		}), 'Path', null, 'path');
	}
});
