/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Editor
 * This window class provide a formable window and allow a mode support so
 * the users can handle easly the status of the data editing since must expect
 * different behaivors for new data from edit actual data.
 */
qx.Class.define('xima.ui.window.Editor', {
	extend: xima.ui.window.Formable,

	statics: {
		NEW_MODE: 0x00,
		EDIT_MODE: 0x01
	},

	properties: {
		mode: {
			check: 'Integer',
			init: 0x00
		}
	},

	members: {
		/**
		 * @inheritdoc
		 */
		getButton: function(id) {
			if(id == 'save') return this.getButton('ok');
			if(this._buttons[id]) return this._buttons[id];

			if(id == 'ok') {
				this._buttons[id] = xima.ui.form.button.Factory.createSave();
				return this._buttons[id];
			}

			return this.base(arguments, id);
		}
	}
});
