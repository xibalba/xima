/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Improved Window class for support the BusyBlocker behavior and change the default
 * caption bar buttons configuration.
 */
qx.Class.define('xima.ui.window.Basic', {
	extend: qx.ui.window.Window,
	include: [ piche.mixins.BusyBlocker ],

	properties : {
		// overridden
		modal : {
			refine : true,
			init : true
		},

		showMaximize : {
			refine : true,
			init : false
		},

		showMinimize : {
			refine : true,
			init : false
		},

		allowMaximize : {
			refine : true,
			init : false
		}
	},

	construct: function(caption, icon) {
		this.base(arguments, caption, icon);
		this.setAllowBusyPopup(true);
		this.getChildControl('icon').set({width: 22, height: 22});
	},

	members: {
		// overridden
		open: function() {
			this.base(arguments);
			this.center();
		}
	}
});
