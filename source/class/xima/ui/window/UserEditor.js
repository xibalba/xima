/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class UserEditor
 * This class provide the window interface for edit all related user data.
 */
qx.Class.define('xima.ui.window.UserEditor', {
	extend: xima.ui.window.Basic,

	construct: function() {
		// @todo add icon
		this.base(arguments, 'Edit user data');

		this.setLayout(new qx.ui.layout.VBox());

		this.add(this.getCompo(), { flex: 1 });
		this.add(this.getToolBar());

		// Hack for ensure center on first appear
		this.addListenerOnce('appear', function(e) {
			this.center();
		}, this);
	},

	members: {
		__compo: null,
		__toolbar: null,

		/**
		 * @param id {String}
		 * @return {qx.ui.groupbox.GroupBox}
		 */
		getBox: function(id) {
			return this.getCompo().getBox(id);
		},

		/**
		 * Retrive the Composite widget of User data.
		 * @return {xima.ui.composite.User}
		 */
		getCompo: function() {
			if(!this.__compo) this.__compo = new xima.ui.composite.User(true);
			return this.__compo;
		},

		/**
		 * Adapter method
		 *
		 * Retrive a user form by the passed id.
		 *
		 * @param id {String}
		 * @return {piche.ui.form.Form}
		 */
		getForm: function(id) {
			return this.getCompo().getForm(id);
		},

		/**
		 * Retrive the toolbar component.
		 *
		 * @return {xima.ui.toolbar.Basic}
		 */
		getToolBar: function() {
			if(this.__toolbar) return this.__toolbar;

			this.__toolbar = new xima.ui.toolbar.Basic();

			this.__toolbar.addSpacer();
			this.__toolbar.composeButton('save');
			this.__toolbar.composeButton('cancel');

			this.__toolbar.getButton('save').setLabel('Save');

			this.__toolbar.getButton('cancel').setLabel('Cancel');
			this.__toolbar.getButton('cancel').addListener("execute", function(e){ this.close(); }, this);

			return this.__toolbar;
		}
	}
});
