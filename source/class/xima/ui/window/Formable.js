/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 *
 */
qx.Class.define('xima.ui.window.Formable', {
	extend: xima.ui.window.Basic,

	construct: function(caption, icon, form) {
		this.base(arguments, caption, icon);
		this._buttons  = {};
		this.setLayout(new qx.ui.layout.Basic());

		form.addButton(this.getButton("ok"));
		form.addButton(this.getButton("cancel"));

		this.add(new qx.ui.form.renderer.Single(form));
		this.setForm(form);
	},

	properties: {
		form: {check: 'qx.ui.form.Form'}
	},

	members: {
		_buttons: null,

		getButton: function(id) {
			if(this._buttons[id]) return this._buttons[id];

			if(id == "ok") this._buttons[id] = xima.ui.form.button.Factory.createOk();
			else if(id == "cancel") {
				this._buttons[id] = xima.ui.form.button.Factory.createCancel();
				this._buttons[id].addListener("execute", function(e){ this.close(); }, this);
			}
			else throw new Error("Unsupported button id.");

			return this._buttons[id];
		}
	}
});
