/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Login UI Window class
 *
 * @asset(xima/icons/system-switch-user.svg)
 * @asset(xima/icons/go-jump.svg)
 */
qx.Class.define('xima.ui.window.Login', {
	extend: xima.ui.window.Basic,

	construct: function() {
		this.base(arguments, 'Login', 'xima/icons/system-switch-user.svg');

		this.set({
			showClose: false,
			allowClose: false,

			width: 320,
			height: 150,

			layout: new qx.ui.layout.VBox()
		});

		this.getChildControl('icon').set({width: 24, height: 24});

		this.add(new qx.ui.form.renderer.Single(this.getForm()));

		this.addListener('appear', function(e) {
			e.getTarget().getForm().focusFirst();
		});
	},

	members: {
		__form: null,
		__btn: null,

		/**
		 * Returns the User Credential UI Form instance or create one and return it if don't exist.
		 * @returns {xima.ui.form.UserCredentials}
		 */
		getForm: function() {
			if(this.__form) return this.__form;

			this.__form = new xima.ui.form.UserCredentials();
			this.__form.addButton(this.getButton());
			return this.__form;
		},

		/**
		 * Returns the UI Login Button instance or create one and return it if don't exist.
		 * @returns {qx.ui.form.Button}
		 */
		getButton: function() {
			if(this.__btn) return this.__btn;

			this.__btn = new qx.ui.form.Button(this.tr('Login'), 'xima/icons/go-jump.svg');
			this.__btn.getChildControl('icon').set({width: 24, height: 24});
			return this.__btn;
		},

		// overriden
		open: function() {
			this.getForm().reset();
			this.base(arguments);
		}
	}
});
