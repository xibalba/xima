/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/application-menu.svg)
 * @asset(xima/icons/configure-shortcuts.svg)
 * @asset(xima/icons/appointment-new.svg)
 * @asset(xima/icons/news-subscribe.svg)
 * @asset(xima/icons/edit-find.svg)
 * @asset(xima/icons/im-user-offline.svg)
 * @asset(xima/icons/im-user-online.svg)
 * @asset(xima/icons/im-user.svg)
 * @asset(xima/icons/user-group-new.svg)
 * @asset(xima/icons/preferences-activities.svg)
 * @asset(xima/icons/system-log-out.svg)
 */
qx.Class.define('xima.ui.Header', {
	extend: qx.ui.container.Composite,

	construct: function() {
		this.base(arguments, new qx.ui.layout.HBox());
		this.__compose();
	},

	members: {
		__titleBar: null,
		__toolBar: null,
		__dropdownBtns: null,
		__menuBtns: null,
		__menues: null,

		/**
		 * Compose the UI arragement of the header interface.
		 * @private
		 */
		__compose: function () {
			// To the left the titlebar
			this.__titleBar = new qx.ui.container.Composite(new qx.ui.layout.HBox());
			this.__titleBar.setAppearance('app-header');
			this.__titleBar.add(new qx.ui.basic.Label('Xima: A web application manager for Mezcal CMS'));

			this.add(this.__titleBar, { flex: 1 });

			// Then the toolbar
			this.add(this.getToolBar());
		},

		/**
		 * This method recompose this view using the permission retrived data.
		 */
		reCompose: function() {
			var permissions = xima.controller.Front.getInstance().getPermissions();

			var allowedConfig = permissions.find(function(el) {
				return el == 'config';
			});

			var allowedTaxo = permissions.find(function(el) {
				return el == 'taxo';
			});

			if(allowedConfig) this.getMenuBtn('settings').show();
			else this.getMenuBtn('settings').exclude();

			if(allowedTaxo) this.getMenuBtn('taxo').show();
			else this.getMenuBtn('taxo').exclude();
		},

		/**
		 * Return the toolbar instance of compose one if do not extist.
		 * @returns {qx.ui.toolbar.ToolBar}
		 */
		getToolBar: function() {
			if(this.__toolBar) return this.__toolBar;

			this.__toolBar = new qx.ui.toolbar.ToolBar();

			this.__toolBar.add(this.getDropdownBtn('app-menu'));
			this.__toolBar.addSeparator();
			this.__toolBar.add(this.getDropdownBtn('user'));

			return this.__toolBar;
		},

		/**
		 * Return a MenuButton instance for the passed id or create one and return it if don't exist.
		 * @param id {String}
		 * @returns {qx.ui.toolbar.MenuButton}
		 */
		getDropdownBtn: function(id) {
			if(!this.__dropdownBtns) this.__dropdownBtns = {};
			if(this.__dropdownBtns[id]) return this.__dropdownBtns[id];

			if(id == 'app-menu') {
				this.__dropdownBtns[id] = new qx.ui.toolbar.MenuButton(null, 'xima/icons/application-menu.svg', this.getMenu(id));
			}
			else if(id == 'user') {
				this.__dropdownBtns[id] = new qx.ui.toolbar.MenuButton('···', 'xima/icons/im-user-offline.svg', this.getMenu(id));
			}

			this.__dropdownBtns[id].getChildControl('icon').set({width: 22, height: 22});
			return this.__dropdownBtns[id];
		},

		/**
		 * Return a Menu Button instance for the passed id or create one and return it if don't exist.
		 * @param id {String}
		 * @returns {qx.ui.menu.Button}
		 */
		getMenuBtn: function(id) {
			if(!this.__menuBtns) this.__menuBtns = {};
			if(this.__menuBtns[id]) return this.__menuBtns[id];

			// Define factories
			switch (id) {
				case 'new-content':
					this.__menuBtns[id] = new qx.ui.menu.Button(this.tr('New Content'), 'xima/icons/document-new.svg');
					break;
				case 'taxo':
					this.__menuBtns[id] = new qx.ui.menu.Button(this.tr('Taxonomy'), 'xima/icons/tag.svg');
					break;
				case 'settings':
					this.__menuBtns[id] = new qx.ui.menu.Button(this.tr('Settings'), 'xima/icons/configure-shortcuts.svg');
					break;
				case 'profile':
					this.__menuBtns[id] = new qx.ui.menu.Button(this.tr('Profile'), 'xima/icons/im-user.svg');
					break;
				case 'logout':
					this.__menuBtns[id] = new qx.ui.menu.Button(this.tr('Logout'), 'xima/icons/system-log-out.svg');
					break;
			}

			// Set id string
			this.__menuBtns[id].setUserData('id', id);

			// Hack the icon size (because I use svg instead of png).
			this.__menuBtns[id].getChildControl('icon').set({width: 22, height: 22});

			// Now can return an useful object
			return this.__menuBtns[id];
		},

		/**
		 * Return a Menu instance of create one and return it if don't exist.
		 * @param id {String}
		 * @returns {qx.ui.menu.Menu}
		 */
		getMenu: function(id) {
			if(!this.__menues) this.__menues = {};
			if(this.__menues[id]) return this.__menues[id];

			if(id == 'app-menu') {
				this.__menues[id] = new qx.ui.menu.Menu();

				this.__menues[id].add(this.getMenuBtn('new-content'));
				this.__menues[id].add(this.getMenuBtn('taxo'));
				this.__menues[id].addSeparator();
				this.__menues[id].add(this.getMenuBtn('settings'));
			}
			else if(id == 'user') {
				this.__menues[id] = new qx.ui.menu.Menu();

				this.__menues[id].add(this.getMenuBtn('profile'));

				this.__menues[id].addSeparator();

				this.__menues[id].add(this.getMenuBtn('logout'));
			}

			return this.__menues[id];
		}
	}
});
