/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/configure.svg)
 */
qx.Class.define("xima.ui.pseudoform.ContentProperties", {
	extend: piche.ui.panel.Basic,

	construct: function() {
		this.base(arguments, 'Properties', 'xima/icons/configure.svg');

		this.setLayout(new qx.ui.layout.VBox(3));
		this.getChildControl('icon').set({width: 22, height: 22});
		this.getChildrenContainer().setPaddingLeft(10);
		this.getChildrenContainer().setPaddingTop(5);

		this.__compo = {};
		this.__selectBoxes = {};
		this.__atoms = {};

		// Add items
		this.add(this.__getCompo('taxo'));
		this.add(this.__getCompo('status'));
		this.add(this.__getAtom("slug"));
		this.add(this.__getCompo("author"));
		this.add(this.__getAtom("editor"));
		this.add(this.__getAtom("createdAt"));
		this.add(this.__getAtom("updatedAt"));
		this.add(this.__getAtom("publishedAt"));
	},

	properties: {
		slug: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		editor: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		createdAt: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		updatedAt: {
			check: "String",
			apply: "__applyAtomLabel",
			nullable: true
		},

		publishedAt: {
			check: "String",
			apply: "__applyPublishedAt",
			nullable: true
		}
	},

	members: {
		__compo: null,
		__selectBoxes: null,
		__atoms: null,

		__getCompo: function(id) {
			if(this.__compo[id]) return this.__compo[id];

				var icon;

			switch(id) {
				case "taxo":
					icon = new qx.ui.basic.Image('xima/icons/tag.svg');
					break;
				case "status":
					icon = new qx.ui.basic.Image('xima/icons/quickopen-class.svg');
					break;
				case "author":
					icon = new qx.ui.basic.Image('xima/icons/im-user.svg');
					break;
				default:
					throw new Error("Unsupported composite id.");
			}

			icon.set({width: 22, height: 22});

			this.__compo[id] = new qx.ui.container.Composite(new qx.ui.layout.HBox());
			this.__compo[id].add(icon);
			this.__compo[id].add(this.getSelectBox(id));

			return this.__compo[id];
		},

		__applyAtomLabel: function(value, old, propertyName) {
			this.__getAtom(propertyName).setLabel(value);
		},

		// Since is possible set a null value, then a «Not yet publishied» must be setted on null.
		__applyPublishedAt: function(value, old, propertyName) {
			if(value) this.__getAtom(propertyName).setLabel(value);
			else this.__getAtom(propertyName).setLabel('Not yet publishied');
		},

		/**
		 * Retrive an Atom instance for any supported `id` to be shown as a content property data.
		 * @param id {String}
		 * @return {qx.ui.basic.Atom}
		 * @private
		 */
		__getAtom: function(id) {
			if(this.__atoms[id]) return this.__atoms[id];

			switch (id) {
				case "slug":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/emblem-symbolic-link.svg');
					this.__atoms[id].setToolTipText("Content slug.");

					break;
				case "editor":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/im-user-away.svg');
					this.__atoms[id].setToolTipText("Username of content editor (last user on edit the content).");

					break;
				case "createdAt":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/view-calendar-special-occasion.svg');
					this.__atoms[id].setToolTipText("Content creation date.");

					break;
				case "updatedAt":
					this.__atoms[id] = new qx.ui.basic.Atom(null, 'xima/icons/view-calendar-upcoming-events.svg');
					this.__atoms[id].setToolTipText("Content last edition date.");

					break;
				case "publishedAt":
					this.__atoms[id] = new qx.ui.basic.Atom("Not yet publishied", 'xima/icons/view-calendar-upcoming-days.svg');
					this.__atoms[id].setToolTipText("Content publication date.");

					break;
				default:
					throw new Error("Unsupported atom id.");
			}

			this.__atoms[id].getChildControl('icon').set({width: 22, height: 22});
			return this.__atoms[id];
		},

		/**
		 * Return a SelectBox instance for any supported id.
		 * @param id {String}
		 * @return {qx.ui.form.SelectBox}
		 */
		getSelectBox: function(id) {
			if(this.__selectBoxes[id]) return this.__selectBoxes[id];

			switch(id) {
				case "taxo":
				case "status":
				case "author":
					this.__selectBoxes[id] = new qx.ui.form.SelectBox();
					break;
				default:
					throw new Error("Unsupported select box id.");
			}

			return this.__selectBoxes[id];
		},

		/**
		 * Clean all internal atom widgets.
		 */
		cleanUp: function() {
			this.setSlug(null);
			this.setEditor(null);
			this.setCreatedAt(null);
			this.setUpdatedAt(null);
			this.setPublishedAt(null);
		}
	}
});
