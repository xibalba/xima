/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Dashboard
 * This class is the main UI element to be shown to users.
 */
qx.Class.define("xima.ui.tabpages.Dashboard", {
	extend: qx.ui.tabview.Page,
	include: [ piche.mixins.BusyBlocker ],

	construct: function() {
		this.base(arguments, 'Dashboard', 'xima/icons/address-book-new.svg');

		this.setLayout(new qx.ui.layout.Grow());
		this.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});

		var splitPane = new qx.ui.splitpane.Pane();
		var contentSplitPane = new qx.ui.splitpane.Pane();

		var compoMain = new qx.ui.container.Composite(new qx.ui.layout.VBox());

		compoMain.add(this.getToolBar());
		compoMain.add(this.getContentList(), {flex: 1});
		compoMain.add(this.getEditor(), {flex: 1});

		contentSplitPane.add(compoMain, 3);
		contentSplitPane.add(this.getPropertiesPane(), 1);

		splitPane.add(this.getFilterPane(), 1);
		splitPane.add(contentSplitPane, 4);

		this.add(splitPane);
	},

	members: {
		__filterPane: null,
		__contentList: null,
		__toolBarC: null,

		__editor: null,

		__propertiesPane: null,

		/**
		 * Return the qx adapter instance for the SimpleMDE editor widget.
		 * @return {xima.ui.mdeditor.Adapter}
		 */
		getEditor: function() {
			if(this.__editor) return this.__editor;

			this.__editor = new xima.ui.mdeditor.Adapter();
			this.__editor.setHeight(280);

			return this.__editor;
		},

		/**
		 * Return the Content Properties Pane instance.
		 * @return {xima.ui.panel.ContentProperties}
		 */
		getPropertiesPane: function() {
			if(this.__propertiesPane) return this.__propertiesPane;

			this.__propertiesPane = new xima.ui.panel.ContentProperties();
			this.__propertiesPane.setShowCloseButton(false);
			this.__propertiesPane.setWidth(240);


			return this.__propertiesPane;
		},

		/**
		 * Return the Filter pane instance or compose one if do not exist.
		 * @return {xima.ui.panel.ContentFilter}
		 */
		getFilterPane: function() {
			if(this.__filterPane) return this.__filterPane;

			this.__filterPane = new xima.ui.panel.ContentFilter();
			return this.__filterPane;
		},

		/**
		 * Return the List widget that must content the loaded content list.
		 * @return {qx.ui.form.List}
		 */
		getContentList: function() {
			if(this.__contentList) return this.__contentList;

			this.__contentList = new qx.ui.form.List();
			return this.__contentList;
		},

		/**
		 * Return the toolbar instance or compose one if do not extist.
		 * @returns {qx.ui.toolbar.ToolBar}
		 */
		getToolBar: function() {
			if(this.__toolBarC) return this.__toolBarC;

			this.__toolBarC = new xima.ui.toolbar.Basic();
			this.__toolBarC.addSpacer();
			this.__toolBarC.composeButton('edit');
			this.__toolBarC.addSpacer();

			return this.__toolBarC;
		}
	}
});
