/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Taxo
 * This class is the Tab UI for manipulate Taxonomy elements.
 */
qx.Class.define("xima.ui.tabpages.Taxo", {
	extend: qx.ui.tabview.Page,

	construct: function() {
		this.base(arguments, 'Taxonomy', 'xima/icons/tag.svg');
		this.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});

		this.setLayout(new qx.ui.layout.HBox());
		this.setShowCloseButton(true);

		this.add(this.getToolBar());
		this.add(this.getTable(), { flex: 1 });
	},

	members: {
		__taxoWinEditor: null,
		__toolbar: null,
		__table: null,

		/**
		 * Return the table widget instance.
		 * @return {piche.ui.table.Simple}
		 */
		getTable: function() {
			if(this.__table) return this.__table;

			this.__table = new piche.ui.table.Simple({
				id: 'Id',
				label: 'Label',
				description: 'Description',
				path: 'Path'
			});

			this.__table.getTableColumnModel().setColumnVisible(0, false);
			this.__table.setColumnWidth('description','2*');

			return this.__table;
		},

		/**
		 * Return the taxonomy window editor instance.
		 * @return {xima.ui.window.Editor}
		 */
		getWinEditor: function() {
			if(this.__taxoWinEditor) return this.__taxoWinEditor;
			this.__taxoWinEditor = new xima.ui.window.Editor('Edit a taxo element', null, new xima.ui.form.Taxo());
			return this.__taxoWinEditor;
		},

		/**
		 * Return the toolbar instance or compose one if do not extist.
		 * @returns {xima.ui.toolbar.Basic}
		 */
		getToolBar: function() {
			if(this.__toolbar) return this.__toolbar;

			this.__toolbar = new xima.ui.toolbar.Basic(true);
			this.__toolbar.composeButton('new');
			this.__toolbar.composeButton('edit');

			return this.__toolbar;
		}
	}
});