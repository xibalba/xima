/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class ContentEditor
 * This class extend the Page class of tabview to compose a UI that allow user to write Content.
 */
qx.Class.define("xima.ui.tabpages.ContentEditor", {
	extend: qx.ui.tabview.Page,
	include: [ piche.mixins.BusyBlocker ],

	construct: function(label) {
		this.base(arguments, label, 'xima/icons/document-new.svg');
		this.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});

		this.set({
			layout: new qx.ui.layout.Grow(),
			showCloseButton: true
		});

		this.add(this.__getSplitPane());

		// Register listeners
		this.getBtnProperties().addListener("execute", this.__handleBtnProperties, this);

		this.getPropertiesPane().getChildControl("close-button").addListener("execute", function(e) {
			this.getBtnProperties().toggleValue();
			this.getPropertiesPane().exclude();
		}, this);
	},

	members: {
		__splitPane: null,
		__editor: null,
		__editorPane: null,
		__propertiesPane: null,

		__btnSave: null,
		__btnProperties: null,

		__titleField: null,

		/**
		 * This method handle the hide/show functionality of properties pane.
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleBtnProperties: function(e) {
			var propertiesView = this.getPropertiesPane();
			if(propertiesView.isVisible()) propertiesView.exclude();
			else propertiesView.show();
		},

		/**
		 * Return the splitpane instance.
		 * If does not existe compose one with the Editor pane at left and the properties pane at right.
		 *
		 * @return {qx.ui.splitpane.Pane}
		 * @private
		 */
		__getSplitPane: function() {
			if(this.__splitPane) return this.__splitPane;

			this.__splitPane = new qx.ui.splitpane.Pane();
			this.__splitPane.add(this.__getEditorPane(), 3);
			this.__splitPane.add(this.getPropertiesPane(), 1);

			return this.__splitPane;
		},

		/**
		 * Return the Editor pane.
		 * If does not exist compose one with the title field and the markdown editor.
		 *
		 * @return {qx.ui.container.Composite}
		 * @private
		 */
		__getEditorPane: function() {
			if(this.__editorPane) return this.__editorPane;

			var editor = this.__getEditor();

			this.__editorPane = new qx.ui.container.Composite(new qx.ui.layout.VBox(5));
			this.__editorPane.add(this.getTitleField());

			this.__editorPane.add(editor, {flex: 1});

			editor.getToolBar().addSpacer();
			editor.getToolBar().add(this.getBtnProperties());
			editor.getToolBar().add(this.getBtnSave());

			return this.__editorPane;
		},

		/**
		 * Return the Xima md Editor.
		 * @return {xima.ui.mdeditor.Editor}
		 * @private
		 */
		__getEditor: function() {
			if(this.__editor) return this.__editor;

			this.__editor = new xima.ui.mdeditor.Editor();
			return this.__editor;
		},

		/**
		 * Return the contentb properties pane instance.
		 * @return {xima.ui.panel.ContentProperties}
		 */
		getPropertiesPane: function() {
			if(this.__propertiesPane) return this.__propertiesPane;

			this.__propertiesPane = new xima.ui.pseudoform.ContentProperties();
			return this.__propertiesPane;
		},

		/**
		 * This method is a shorthand for retrive the taxonomy and status SelectBoxes
		 * instance of the properties pane.
		 *
		 * @param id {String}
		 * @return {qx.ui.form.SelectBox}
		 */
		getSelectBox: function(id) {
			return this.getPropertiesPane().getSelectBox(id);
		},

		/**
		 * Return the save button instance.
		 * This button must be attached to the editor toolbar.
		 *
		 * @return {qx.ui.toolbar.Button}
		 */
		getBtnSave: function() {
			if(this.__btnSave) return this.__btnSave;

			this.__btnSave = xima.ui.toolbar.button.Factory.createSave();
			return this.__btnSave;
		},

		/**
		 * Return the toggable button for hide/show the properties pane.
		 * This button must be attached to the editor toolbar.
		 *
		 * @return {qx.ui.toolbar.CheckBox}
		 */
		getBtnProperties: function() {
			if(this.__btnProperties) return this.__btnProperties;

			this.__btnProperties = xima.ui.toolbar.button.Factory.createProperties().set({value: true});
			return this.__btnProperties;
		},

		/**
		 * Return the content title field instance.
		 * @return {qx.ui.form.TextField}
		 */
		getTitleField: function () {
			if(this.__titleField) return this.__titleField;

			this.__titleField = new qx.ui.form.TextField().set({
				placeholder: "Content Title"
			});

			return this.__titleField;
		},

		/**
		 * This method is a shorthand for retrive the editor value.
		 * @return {String}
		 */
		getBody: function () {
			return this.__getEditor().getValue();
		},

		/**
		 * This method is a shorthand for set a string value to the editor.
		 * @param value {String}
		 */
		setBody: function(value) {
			this.__getEditor().setValue(value);
		}
	}
});