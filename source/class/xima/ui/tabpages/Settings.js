/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Class Settigs
 * This class is the Tab UI for manipulate application settings:
 *
 *   * General (todo)
 *   * Roles
 */
qx.Class.define("xima.ui.tabpages.Settings", {
	extend: piche.ui.tabview.Page,

	construct: function() {
		this.base(arguments, 'Settings', 'xima/icons/configure-shortcuts.svg');
		this.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});

		this.__tabs = {};

		this.setLayout(new qx.ui.layout.Grow());
		this.setShowCloseButton(true);

		this.add(this.getTabView());
	},

	members: {
		__tabView: null,
		__tabs: null,

		getTabView: function() {
			if(this.__tabView) return this.__tabView;

			this.__tabView = new qx.ui.tabview.TabView('left');

			// Compose settings tabs
			this.__tabView.add(this.getTab('params'));
			this.__tabView.add(this.getTab('users'));
			//this.__tabView.add(this.getTab('roles'));

			return this.__tabView;
		},

		getTab: function(id) {
			if(this.__tabs[id]) return this.__tabs[id];

			if(id == 'roles') this.__tabs[id] = new xima.ui.tabpages.Roles();
			else if(id == 'users') this.__tabs[id] = new xima.ui.tabpages.Users();
			else if(id == 'params') this.__tabs[id] = new xima.ui.tabpages.Params();
			else throw new Error('Unsupported tab id.');

			return this.__tabs[id];
		}
	}
});