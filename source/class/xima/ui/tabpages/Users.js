/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Class.define("xima.ui.tabpages.Users", {
	extend: qx.ui.tabview.Page,

	construct: function() {
		this.base(arguments, 'Users', 'xima/icons/user-group-new.svg');
		this.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});
		this.setLayout(new qx.ui.layout.HBox());
	},

	members: {
		__rolesList: null,
		__splitPane: null,
		__table: null,
		__toolbar: null,
		__winEditor: null,

		compose: function() {
			this.add(this.getToolBar());
			this.add(this.__getSplitPane(), { flex: 1 });
		},

		/**
		 * Retrive the list widgets that show the roles of selected user.
		 *
		 * @return {qx.ui.form.List}
		 */
		getRolesList: function() {
			if(!this.__rolesList) this.__rolesList = new piche.ui.form.List();
			return this.__rolesList;
		},

		__getSplitPane: function() {
			if(this.__splitPane) return this.__splitPane;

			this.__splitPane = new qx.ui.splitpane.Pane();
			this.__splitPane.add(this.getTable(), 3);
			this.__splitPane.add(this.getRolesList(), 1);

			return this.__splitPane;
		},

		/**
		 * Return the table widget instance.
		 * @return {piche.ui.table.Simple}
		 */
		getTable: function() {
			if(this.__table) return this.__table;

			this.__table = new piche.ui.table.Simple({
				username: 'Username',
				email: 'Email',
				fullname: 'Name',
				active: 'Active'
			});

			this.__table.getTableColumnModel().setDataCellRenderer(3, new qx.ui.table.cellrenderer.Boolean());

			this.__table.setColumnWidth('email','2*');
			this.__table.setColumnWidth('fullname','2*');

			return this.__table;
		},

		/**
		 * Return the taxonomy window editor instance.
		 * @return {xima.ui.window.Editor}
		 */
		getWinEditor: function() {
			if(!this.__winEditor) this.__winEditor = new xima.ui.window.UserEditor();
			return this.__winEditor;
		},

		/**
		 * Return the toolbar instance or compose one if do not extist.
		 * @returns {xima.ui.toolbar.Basic}
		 */
		getToolBar: function() {
			if(this.__toolbar) return this.__toolbar;

			this.__toolbar = new xima.ui.toolbar.Basic(true);
			this.__toolbar.composeButton('new');
			this.__toolbar.composeButton('edit');

			return this.__toolbar;
		}
	}
});