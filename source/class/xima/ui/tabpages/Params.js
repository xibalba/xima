/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/story-editor.svg)
 */
qx.Class.define("xima.ui.tabpages.Params", {
	extend: qx.ui.tabview.Page,

	construct: function() {
		this.base(arguments, 'Params', 'xima/icons/story-editor.svg');
		this.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});
		this.setLayout(new qx.ui.layout.HBox(6));

		this.compose();
	},

	members: {
		__form: null,
		__toolbar: null,

		compose: function() {
			this.add(this.getToolBar());
			this.add(new qx.ui.form.renderer.Single(this.getForm()));
		},

		getForm: function() {
			if(this.__form) return this.__form;

			this.__form = new piche.ui.form.Form();

			this.__form.add(new qx.ui.form.TextField().set({
				required: true,
				width: 300,
				requiredInvalidMessage: 'Required'
			}), 'Title', null, 'title');

			return this.__form;
		},

		/**
		 * Return the toolbar instance or compose one if do not extist.
		 * @returns {xima.ui.toolbar.Basic}
		 */
		getToolBar: function() {
			if(this.__toolbar) return this.__toolbar;

			this.__toolbar = new xima.ui.toolbar.Basic(true);
			this.__toolbar.composeButton('save');

			return this.__toolbar;
		}
	}
});
