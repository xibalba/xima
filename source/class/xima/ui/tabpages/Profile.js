/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Class.define('xima.ui.tabpages.Profile', {
	extend: qx.ui.tabview.Page,
	include: [ piche.mixins.BusyBlocker ],

	construct: function() {
		this.base(arguments, 'Profile', 'xima/icons/im-user.svg');
		this.getChildControl('button').getChildControl('icon').set({width: 22, height: 22});

		this.setLayout(new qx.ui.layout.VBox());
		this.setShowCloseButton(true);

		this.add(this.getToolBar());
		this.add(this.getCompo(), { flex: 1 });
	},

	members: {
		__toolbar: null,
		__compo:  null,
		__window: null,

		getCompo: function() {
			if(this.__compo) return this.__compo;

			this.__compo = new xima.ui.composite.User();

			// Profile must have user field as read only
			this.__compo.getForm('user').getItem('username').setReadOnly(true);
			return this.__compo;
		},

		// Adapter method
		getForm: function(id) {
			return this.getCompo().getForm(id);
		},

		getToolBar: function() {
			if(this.__toolbar) return this.__toolbar;

			this.__toolbar = new xima.ui.toolbar.Basic();

			this.__toolbar.addSpacer();
			this.__toolbar.composeButton('save');
			this.__toolbar.addSeparator();
			this.__toolbar.composeButton('change-pass');
			this.__toolbar.addSpacer();

			return this.__toolbar;
		},

		getWindow: function() {
			if(this.__window) return this.__window;

			this.__window = new xima.ui.window.Formable(
				'Change password',
				'xima/icons/database-change-key.svg',
				this.getForm('password')
			);

			return this.__window;
		}
	}
});