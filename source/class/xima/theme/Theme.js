/* ****************************************************************

 @link https://bitbucket.org/xibalba/xima
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Xima: Mezcal CMS client application --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Theme.define("xima.theme.Theme", {
	meta : {
		color : xima.theme.Color,
		decoration : xima.theme.Decoration,
		font : xima.theme.Font,
		icon : qx.theme.icon.Oxygen,
		appearance : xima.theme.Appearance
	}
});
