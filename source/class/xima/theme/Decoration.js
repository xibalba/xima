/* ****************************************************************

 @link https://bitbucket.org/xibalba/xima
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Xima: Mezcal CMS client application --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

qx.Theme.define("xima.theme.Decoration", {
	extend: piche.theme.Decoration
});
