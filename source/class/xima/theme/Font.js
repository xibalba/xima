/* ****************************************************************

 @link https://bitbucket.org/xibalba/xima
 @copyright 2015 Xibalba Lab
 @license LGPL: http://www.gnu.org/licenses/lgpl.html

 -- Xima: Mezcal CMS client application --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(font/RobotoSlab-Regular.ttf)
 */
qx.Theme.define("xima.theme.Font", {
	extend: piche.theme.Font,
	fonts: {
		"mdeditor": {
			size: 14,

			family: ["RobotoSlab"],
			sources: [{
				family : "RobotoSlab",
				source: [
					"font/RobotoSlab-Regular.ttf"
				]
			}]
		}
	}
});
