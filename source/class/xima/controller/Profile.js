/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Profile controller.
 */
qx.Class.define('xima.controller.Profile', {
	extend: piche.controller.BaseAbstract,
	type : "singleton",

	construct: function() {
		this.base(arguments);
		this.__usersController = new qx.data.controller.Form(null, this.getView().getForm('user'));
		this.__personController = new qx.data.controller.Form(null, this.getView().getForm('person'));
		this.__passwordController = new qx.data.controller.Form(null, this.getView().getForm('password'));

		this.__usersController.createModel();
		this.__personController.createModel();
		this.__passwordController.createModel();

		// Listeners

		var toolbar = this.getView().getToolBar();

		toolbar.getButton('change-pass').addListener('execute', function(e) {
			this.__passwordController.resetModel();
			this.__passwordController.createModel();
			this.getView().getWindow().open();
		}, this);

		toolbar.getButton('save').addListener('execute', this.__handleBtnSaveTap, this);

		this.getView().getWindow().getButton('ok').addListener('execute', this.__handleBtnOkTap, this);
	},

	members: {
		__usersController: null,
		__personController: null,
		__passwordController:  null,

		__restResource: null,
		__view: null,

		/**
		 * Return the rest resource object.
		 * @return {xima.io.rest.Resource}
		 * @private
		 */
		__getRestResource: function() {
			if(this.__restResource) return this.__restResource;

			this.__restResource = new xima.io.rest.Resource({
				put: {method: 'PUT', url: "session/profile" },
				putPassword: {method: 'PUT', url: "session/password" }
			});

			this.__restResource.addListener('putSuccess', this.__handlePutSuccess, this);
			this.__restResource.addListener('putPasswordSuccess', this.__handlePutPasswordSuccess, this);

			return this.__restResource;
		},

		__handleBtnSaveTap: function(e) {
			var userModel = this.__usersController.getModel();
			var personModel = this.__personController.getModel();

			var userData = {
				username: userModel.get('username'),
				email: userModel.get('email')
			};

			var personData = {
				did: personModel.get('did'),
				name: personModel.get('name'),
				surname: personModel.get('surname')
			};

			this.getView().block();

			this.__getRestResource().put(null, {
				"user": qx.util.Serializer.toJson(userData),
				"person": qx.util.Serializer.toJson(personData)
			});
		},

		__handleBtnOkTap: function(e) {
			var passModel = this.__passwordController.getModel();
			var userModel = this.__usersController.getModel();

			if(passModel.get('prepassword') !== passModel.get('newpassword')) {
				piche.ui.Dialog.error("New password verification does not match.");
			}
			else {
				var hasher = new jsSHA(userModel.get('username') + passModel.get('newpassword'), "TEXT");

				this.getView().getWindow().block();
				this.__getRestResource().putPassword(null, { newpassword: hasher.getHash("SHA-256", "HEX") });
			}
		},

		__handlePutSuccess: function(restEvent) {
			var profile = restEvent.getRequest().getResponse()['profile'];
			this.getView().unblock();
			xima.controller.Front.getInstance().setProfileData(profile);
		},

		__handlePutPasswordSuccess: function(restEvent) {
			this.getView().getWindow().unblock();
			this.getView().getWindow().close();
		},

		/**
		 * Return the view instance.
		 * @return {xima.ui.tabpages.Taxo}
		 */
		getView: function() {
			if(this.__view) return this.__view;

			this.__view = new xima.ui.tabpages.Profile();
			return this.__view;
		},

		dispatch: function() {
			var profile = xima.controller.Front.getInstance().getProfileData();

			this.__usersController.getModel().set(profile['user']);
			this.__personController.getModel().set(profile['person']);
		}
	}
});