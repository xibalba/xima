/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * User controller.
 * This controller implements the singleton pattern.
 * This controller take care of handle the interections of the user with the UI elements
 * of the users tab page view.
 */
qx.Class.define('xima.controller.User', {
	extend: piche.controller.BaseAbstract,
	type : "singleton",

	construct: function() {
		this.base(arguments);

		this.dispatch();
	},

	members: {
		__restResource: null,
		__view: null,

		/**
		 * Return the rest resource object.
		 * @return {xima.io.rest.Resource}
		 * @private
		 */
		__getRestResource: function() {
			if(this.__restResource) return this.__restResource;

			this.__restResource = new xima.io.rest.Resource({
				post: {method: 'POST', url: "user"},
				put: {method: 'PUT', url: "user/{id}" },
				get: {method: 'GET', url: "user" }
			});

			this.__restResource.addListener('getSuccess', this.__handleGetSuccess, this);
			//this.__restResource.addListener('putSuccess', this.__handlePutSuccess, this);
			//this.__restResource.addListener('postSuccess', this.__handlePostSuccess, this);

			return this.__restResource;
		},

		/**
		 * Handle the process for a successful GET request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handleGetSuccess: function(restEvent) {
			var response = restEvent.getRequest().getResponse();
			this.getView().getTable().getTableModel().setDataAsMapArray(response['users']);
			console.info('Hacer la tecnología');
		},

		/**
		 * Return the view instance.
		 * @return {xima.ui.tabpages.User}
		 */
		getView: function() {
			if(this.__view) return this.__view;

			this.__view = new xima.ui.tabpages.User();
			return this.__view;
		},

		dispatch: function() {
			this.__getRestResource().get();
		}
	}
});