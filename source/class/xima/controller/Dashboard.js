/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * @asset(xima/icons/tag.svg)
 * @asset(xima/icons/news-subscribe.svg)
 */
qx.Class.define('xima.controller.Dashboard', {
	extend: piche.controller.BaseAbstract,
	type: 'singleton',

	construct: function() {
		this.base(arguments);

		var view = this.getView();

		var selectBoxTaxo = view.getFilterPane().getSelectBox('taxo');
		var selectBoxStatus = view.getFilterPane().getSelectBox("status");

		// Create a list controller for Taxonomy list
		this.__taxoListController = new qx.data.controller.List(null, selectBoxTaxo, "label");
		this.__statusListController = new qx.data.controller.List(null, selectBoxStatus, "label");
		this.__contentListController = new qx.data.controller.List(null, view.getContentList(), "title");

		this.__contentListController.setDelegate({
			createItem: function () {
				var item = new qx.ui.form.ListItem(null, 'xima/icons/news-subscribe.svg');
				item.getChildControl('icon').set({width: 22, height: 22});
				return item;
			}
		});

		// The data is returned as Json by server.
		var store = new qx.data.store.Rest(this.__getRestResource(), "getTaxo");
		store.bind("model.taxo", this.__taxoListController, "model");

		var statusStore = new qx.data.store.Rest(this.__getRestResource(), "getStatus");
		statusStore.bind("model.status", this.__statusListController, "model");

		var contentStore = new qx.data.store.Rest(this.__getRestResource(), "getContent");
		contentStore.bind("model.content", this.__contentListController, "model");

		// -----------------------

		// Register listeners
		view.getFilterPane().getToolBar().getButton('filter').addListener("execute", this.__handleFilter, this);
		view.getContentList().addListener("changeSelection", this.__handleContentListSelection, this);

		view.getToolBar("content").getButton("edit").addListener("execute", this.__handleEditContentBtnTap, this);
	},

	members: {
		__taxoListController: null,
		__statusListController: null,
		__contentListController: null,

		__restResource: null,

		__view: null,

		__getRestResource: function() {
			if(this.__restResource) return this.__restResource;

			this.__restResource = new xima.io.rest.Resource({
				getTaxo: { method: 'GET', url: "taxo" },
				getStatus: { method: 'GET', url: "params/contentStatus" },
				getContent: { method: 'GET', url: "content" }
			});

			// Add listeners
			//this.__restResource.addListener("putSuccess", this.__handlePutSuccess, this);
			this.__restResource.addListener("getTaxoSuccess", function(e) {
				e.getRequest().getResponse()['taxo'].unshift({
					id: "all",
					label: "All"
				});
			}, this);

			this.__restResource.addListener("getStatusSuccess", function(e) {
				e.getRequest().getResponse()['status'].unshift({
					id: "all",
					label: "All"
				});
			});

			this.__restResource.addListener("getContentSuccess", function(e) {
				this.getView().unblock();
			}, this);

			// @todo process error

			return this.__restResource;
		},

		/**
		 * Handle the logic when a user tap the edit content button.
		 *
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleEditContentBtnTap: function (e) {
			var selectedContent = this.__contentListController.getSelection().getItem(0);
			if(selectedContent) {
				var frontView = xima.controller.Front.getInstance().getView();
				var controller = new xima.controller.ContentEditor();
				var newEditor = controller.getView();

				frontView.add(newEditor);
				frontView.setSelection([newEditor]);

				controller.dispatch(selectedContent.getUuid());
			}
		},

		__handleFilter: function(e) {
			var filterPane = this.getView().getFilterPane();
			var taxoModel = filterPane.getSelectBox('taxo').getSelection()[0].getModel();
			var statusModel = filterPane.getSelectBox('status').getSelection()[0].getModel();

			var rest = this.__getRestResource();
			var criteria = {};

			if(taxoModel.getId() !== 'all') criteria['taxo_id'] = taxoModel.getId();
			if(statusModel.getId() !== 'all') criteria['status'] = statusModel.getId();

			this.getView().block();
			rest.getContent(null, { criteria: qx.util.Serializer.toJson(criteria) });
		},

		__handleContentListSelection: function(e) {
			var data = e.getData();
			if(data[0]) {
				var model = data[0].getModel();

				var taxoModel = this.__taxoListController.getModel();
				var statusModel = this.__statusListController.getModel();

				var taxoLabel = '';
				var statusLabel = '';

				taxoModel.forEach(function(el) {
					if(model.get('taxo_id') == el.get('id')) {
						taxoLabel = el.get('label');
						return;
					}

				});

				statusModel.forEach(function(el) {
					if(model.get('status') == el.get('id')) {
						statusLabel = el.get('label');
						return;
					}
				});

				this.getView().getPropertiesPane().setTaxo(taxoLabel);
				this.getView().getPropertiesPane().setStatus(statusLabel);
				this.getView().getPropertiesPane().setSlug(model.get('slug'));

				this.getView().getPropertiesPane().setAuthor(model.get('author'));
				this.getView().getPropertiesPane().setEditor(model.get('editor'));

				this.getView().getPropertiesPane().setCreatedAt(model.get('created_at'));
				this.getView().getPropertiesPane().setUpdatedAt(model.get('updated_at'));
				this.getView().getPropertiesPane().setPublishedAt(model.get('published_at'));

				this.getView().getEditor().getEditor().value(model.getBody());
			}
			else {
				this.getView().getPropertiesPane().cleanUp();
				this.getView().getEditor().getEditor().value("");
			}
		},

		/**
		 * Return the tab page ui instance.
		 *
		 * @return {xima.ui.tabpages.Dashboard}
		 */
		getView: function() {
			if(this.__view) return this.__view;
			this.__view = new xima.ui.tabpages.Dashboard();
			return this.__view;
		},

		dispatch: function() {
			this.__contentListController.resetModel();
			this.__statusListController.resetModel();
			this.__statusListController.resetModel();

			this.__getRestResource().getTaxo();
			this.__getRestResource().getStatus();
		}
	}
});