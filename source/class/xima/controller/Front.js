/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Front
 * This is the Front controller, handle the Fron UI events.
 *
 * @asset(qx/icon/Oxygen/48/status/security-high.png)
 * @asset(qx/icon/Oxygen/16/actions/help-about.png)
 */
qx.Class.define('xima.controller.Front', {
	extend: piche.controller.BaseAbstract,
	type: 'singleton',

	construct: function() {
		this.base(arguments);

		this.__profileData = {};
		this.__controllers = {};
		this.__permissions = {};

		this.__loginFormController  = new qx.data.controller.Form(null, this.getView().getWindow('login').getForm(), true);
		this.__loginFormController.createModel();

		this.getView().getWindow('login').getButton()
			.addListener('execute', this.__handleLoginBtnTap, this);

		// Register listener for header's toolbar buttons.
		this.getAppInstance().getHeader().getMenuBtn('logout')
			.addListener('execute', this.__handleLogout, this);

		this.getAppInstance().getHeader().getMenuBtn('new-content')
			.addListener('execute', this.__handleNewContentMenuBtnTap, this);

		this.getAppInstance().getHeader().getMenuBtn('settings')
			.addListener('execute', this.__handleSettingsMenuBtnTap, this);

		this.getAppInstance().getHeader().getMenuBtn('taxo')
			.addListener('execute', this.__handleTaxoMenuBtnTap, this);

		this.getAppInstance().getHeader().getMenuBtn('profile')
			.addListener('execute', this.__handleProfileMenuBtnTap, this);
	},

	members: {
		__profileData: null,

		// Map for controllers
		__controllers: null,
		__permissions: null,

		// Login have it's own controller
		__loginFormController: null,
		__view: null,
		__sessIdCookie: null,
		__restResource: null,

		/**
		 * Return the application instance.
		 * This method is just a shortcut for `qx.core.Init.getApplication()`.
		 *
		 * @return {xima.Application}
		 */
		getAppInstance: function() {
			return qx.core.Init.getApplication();
		},

		getProfileData: function() {
			return this.__profileData;
		},

		setProfileData: function(profile) {
			this.__profileData = profile;
		},

		/**
		 * Return a regular controller identified by an `id`.
		 *
		 * @param {String} id
		 * @return {piche.controller.BaseAbstract}
		 *
		 * @private
		 */
		__getController: function(id) {
			if(this.__controllers[id]) return this.__controllers[id];

			if(id === 'dashboard') this.__controllers[id] = new xima.controller.Dashboard();
			else if (id === 'content') this.__controllers[id] = new xima.controller.ContentEditor();

			return this.__controllers[id];
		},

		/**
		 * Return the Rest Resource instance.
		 *
		 * @return {xima.io.rest.Resource}
		 * @protected
		 */
		__getRestResource: function() {
			if(this.__restResource) return this.__restResource;

			this.__restResource = new xima.io.rest.Resource({
				getProfile: { method: 'GET',  url: 'session/profile' },
				getPermissions: { method: 'GET',  url: 'session/permissions' }
			});

			this.__restResource.addListener('getProfileSuccess', function(e) {
				var profile = e.getRequest().getResponse()['profile'];
				this.__profileData = profile;

				var btnMenu = this.getAppInstance().getHeader().getDropdownBtn('user');
				btnMenu.setLabel(profile['user']['username']);
				btnMenu.setIcon('xima/icons/im-user-online.svg');
			}, this);

			this.__restResource.addListener('getPermissionsSuccess', function(e) {
				this.__permissions = e.getRequest().getResponse()['permissions'];
				this.getView().unblock();
				this.__composeViews();
			}, this);

			return this.__restResource;
		},

		/**
		 * Check if exist the PHPSESSID cookie to know if exist some session or not.
		 * @protected
		 */
		_checkSession: function() {
			if(this.__sessIdCookie) return this.__sessIdCookie;

			this.__sessIdCookie = qx.bom.Cookie.get('PHPSESSID');
			return this.__sessIdCookie;
		},

		/**
		 * Handle the logic for request a login action.
		 * Ensure that `PHPSESSID` cookie must nod exist and listen success and failure
		 * and process it.
		 *
		 * This method must be attached to the listener of the Window login.
		 *
		 * @protected
		 */
		__handleLoginBtnTap: function() {
			var win = this.getView().getWindow('login');
			var form = win.getForm();

			if(form.validate()) {
				this.__loginFormController.updateModel();
				var model = this.__loginFormController.getModel();

				qx.bom.Cookie.del('PHPSESSID', '/');
				this.__sessIdCookie = null;

				var req = new xima.io.Xhr('session', 'POST');
				req.setAuthentication(new xima.io.BasicAuth(model.getUser(), model.getPassword()));

				req.addListener('statusError', function(e) {
					win.unblock();

					var httpStatusCode = e.getTarget().getStatus();
					console.info(httpStatusCode );

					// handle 40X errors
					if (httpStatusCode == 401) piche.ui.Dialog.error('Authentication failed.');
					else if (httpStatusCode == 403) piche.ui.Dialog.error('Server refuse client request.');
					else piche.ui.Dialog.error('Unexpected error.');
				});

				//Escuchar lo éxitos
				req.addListener("success", function (e) {
					win.unblock();

					var response = e.getTarget().getResponse();

					if (response.success) {
						win.close();

						this.getView().block();
						this.__getRestResource().getProfile();
						this.__getRestResource().getPermissions();
					}
					else {
						piche.ui.Dialog.error(
							'Incompatible version. ' +
							'Please reload the application.'
						);

						console.error('Server refuse client version.');
					}
				}, this);

				win.block();
				req.send();
			}
		},

		/**
		 * Handle the logout logic.
		 * Requet a DELETE method and listen by success and failure and process it.
		 *
		 * This method must be assigned to the Listener of the Menu Button for logout.
		 *
		 * @protected
		 */
		__handleLogout: function() {
			// @todo add confirmation dialog
			var req = new xima.io.Xhr('session', 'DELETE');

			req.addListener("statusError", function(e, f) {
				piche.ui.Dialog.error('Unexpected error.');
			});

			req.addListener('success', function(e) {
				qx.bom.Cookie.del('PHPSESSID', '/');
				this.getView().cleanUp();
				this.getAppInstance().getHeader().getDropdownBtn('user').setLabel('');
				this.getView().getWindow('login').open();
			}, this);

			req.send();
		},

		/**
		 * Handle the creation of a new TabPage of ContentEditor and push the new controller into .
		 *
		 * @param {qx.event.type.Event} e
		 * @protected
		 */
		__handleNewContentMenuBtnTap: function(e) {
			var controller = new xima.controller.ContentEditor();

			var newEditor = controller.getView();
			this.getView().add(newEditor);
			this.getView().setSelection([newEditor]);

			controller.dispatch();
		},

		__handleTaxoMenuBtnTap: function(e) {
			var taxoView = xima.controller.Taxo.getInstance().getView();

			if(this.getView().indexOf(taxoView) === -1) this.getView().add(taxoView);
			this.getView().setSelection([taxoView]);

			xima.controller.Taxo.getInstance().dispatch();
		},

		__handleProfileMenuBtnTap: function(e) {
			var profileView = xima.controller.Profile.getInstance().getView();
			if(this.getView().indexOf(profileView) === -1) this.getView().add(profileView);
			this.getView().setSelection([profileView]);
			xima.controller.Profile.getInstance().dispatch();
		},

		__handleSettingsMenuBtnTap: function(e) {
			var settingsView = xima.controller.Settings.getInstance().getView();
			if(this.getView().indexOf(settingsView) === -1) this.getView().add(settingsView);
			this.getView().setSelection([settingsView]);
		},

		__composeViews: function() {
			this.getAppInstance().getHeader().reCompose();
			this.getView().add(xima.controller.Dashboard.getInstance().getView());
			xima.controller.Dashboard.getInstance().dispatch();
		},

		/**
		 * check if a session exist, when thats true then compose the app UI,
		 * otherwise show the window login.
		 */
		dispatch: function() {
			if(this._checkSession()) {
				this.getView().block();
				this.__getRestResource().getProfile();
				this.__getRestResource().getPermissions();
			}
			else this.getView().getWindow('login').open();
		},

		/**
		 * Return the front view.
		 * @return {xima.view.Dashboard}
		 */
		getView: function() {
			if(this.__view) return this.__view;

			this.__view = new xima.ui.Front();
			return this.__view;
		},

		/**
		 * Retrieve the permissions array of actual session.
		 * @return {Array}
		 */
		getPermissions: function() {
			return this.__permissions;
		}
	}
});
