/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * ContentEditor controller.
 * As any controller, this class follow a kind of MVC pattern.
 * This class compose the view at construction time, the view is build by the internat `getView()`
 * factory method.
 *
 * This controller folw an internal state for determinate if has or not a disrty data, so,
 * if there is some dirty data then rise a Confirm dialog befoore close (@todo).
 */
qx.Class.define('xima.controller.ContentEditor', {
	extend: piche.controller.BaseAbstract,

	construct: function() {
		this.base(arguments);

		// Register listeners
		this.getView().getBtnSave().addListener("execute", this.__handleBtnSave, this);
		this.getView().addListener("close", this.__handleCloseView, this);
	},

	members: {
		__restResource: null,
		__view: null,

		// Internal list controllers for taxo, status & users SelectBox
		__taxoListController: null,
		__statusListController: null,
		__usersListController: null,

		// Content id
		__contentId: null,

		__propertiesFlag: null,

		/**
		 * Initialize the internal list controllers for the taxonomy, status and users SelectBox widgets.
		 * @return {qx.data.controller.List}
		 */
		dispatch: function(uuid) {
			this.__propertiesFlag = 0;

			this.__getRestResource().addListenerOnce('getTaxoSuccess', function(restEvent) {
				this.__propertiesFlag++;
				this.__invokeGet();
			}, this);

			this.__getRestResource().addListenerOnce('getStatusSuccess', function(restEvent) {
				this.__propertiesFlag++;
				this.__invokeGet();
			}, this);

			this.__getRestResource().addListenerOnce('getUserSuccess', function(restEvent) {
				this.__propertiesFlag++;
				this.__invokeGet();
			}, this);

			this.__taxoListController = new qx.data.controller.List(null, this.getView().getSelectBox("taxo"), "label");
			this.__statusListController = new qx.data.controller.List(null, this.getView().getSelectBox("status"), "label");
			this.__usersListController = new qx.data.controller.List(null, this.getView().getSelectBox("author"), "username");

			var taxoStore = new qx.data.store.Rest(this.__getRestResource(), "getTaxo");
			var statusStore = new qx.data.store.Rest(this.__getRestResource(), "getStatus");
			var usersStore = new qx.data.store.Rest(this.__getRestResource(), "getUsers");

			taxoStore.bind("model.taxo", this.__taxoListController, "model");
			statusStore.bind("model.status", this.__statusListController, "model");
			usersStore.bind("model.users", this.__usersListController, "model");

			// Execute request for populate SelectBoxes
			this.__getRestResource().getTaxo();
			this.__getRestResource().getStatus();
			this.__getRestResource().getUsers();

			if(uuid) {
				this.__propertiesFlag = 3;
				this.__contentId = uuid;
				this.getView().block();
			}
		},

		__invokeGet: function() {
			if(this.__propertiesFlag > 2) this.__getRestResource().get(null, {id: this.__contentId});
		},

		/**
		 * Return the rest resource object.
		 * @return {xima.io.rest.Resource}
		 * @private
		 */
		__getRestResource: function() {
			if(this.__restResource) return this.__restResource;

			this.__restResource = new xima.io.rest.Resource({
				post: {method: 'POST', url: "content"},
				put: {method: 'PUT', url: "content/{id}" },
				get: {method: 'GET', url: "content" },

				getTaxo: { method: 'GET', url: "taxo" },
				getStatus: { method: 'GET', url: "params/contentStatus" },
				getUsers: {method: 'GET', url: "users"}
			});

			// Add listeners
			this.__restResource.addListener("getSuccess", this.__handleContentResponseSuccess, this);
			this.__restResource.addListener("postSuccess", this.__handleContentResponseSuccess, this);
			this.__restResource.addListener("putSuccess", this.__handleContentResponseSuccess, this);

			return this.__restResource;
		},

		/**
		 * Handle the process for a successful POST and PUT request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handleContentResponseSuccess: function(restEvent) {
			var content = restEvent.getRequest().getResponse()['content'];
			var view = this.getView();
			var titleField = view.getTitleField();

			this.__contentId = content['uuid'];

			view.setLabel(content['title']);
			view.getPropertiesPane().setSlug(content['slug']);
			view.getPropertiesPane().setEditor(content['editor']);
			view.getPropertiesPane().setCreatedAt(content['created_at']);
			view.getPropertiesPane().setUpdatedAt(content['updated_at']);
			view.getPropertiesPane().setPublishedAt(content['published_at']);

			// Set diff values
			if(titleField.getValue() !== content['title']) titleField.setValue(content['title']);
			if(view.getBody() !== content['body']) view.setBody(content['body']);

			var taxoSelection = this.getListSelection('taxo');
			var statusSelection = this.getListSelection('status');
			var userSelection = this.getListSelection('author');

			//console.info(userSelection);

			if(taxoSelection && taxoSelection.get('id') !== content['taxo_id']) {
				var taxoModel = this.__taxoListController.getModel();
				taxoModel.forEach(function(el) {
					if(el.get('id') == content['taxo_id']) {
						var newSelection = new qx.data.Array();
						newSelection.push(el);
						this.__taxoListController.setSelection(newSelection);
					}
				}, this);
			}

			if(statusSelection && statusSelection.get('id') !== content['status']) {
				var statusModel = this.__statusListController.getModel();
				statusModel.forEach(function(el) {
					if(el.get('id') == content['status']) {
						var newSelection = new qx.data.Array();
						newSelection.push(el);
						this.__statusListController.setSelection(newSelection);
					}
				}, this);
			}

			if(userSelection && userSelection.get('username') !== content['author']) {
				var userModel = this.__usersListController.getModel();
				userModel.forEach(function(el) {
					if(el.get('username') == content['author']) {
						var newSelection = new qx.data.Array();
						newSelection.push(el);
						this.__usersListController.setSelection(newSelection);
					}
				}, this);
			}

			this.getView().unblock();
		},

		/**
		 * Handle the logic for the Save Button.
		 * Take all editable elements, prepare a data object and execute the rest request.
		 * @private
		 */
		__handleBtnSave: function() {
			var content = qx.util.Serializer.toJson({
				title: this.getView().getTitleField().getValue(),
				taxo_id: this.getListSelection('taxo').get('id'),
				status: this.getListSelection('status').get('id'),
				author: this.getListSelection('author').get('username'),
				body: this.getView().getBody()
			});

			this.getView().block();
			if(this.__contentId) this.__getRestResource().put({id: this.__contentId}, {content: content});
			else this.__getRestResource().post(null, {content: content});
		},

		__handleCloseView: function() {
			this.dispose();
		},

		/**
		 * Return the selection model by id for Taxonomy or status.
		 * @param id {String}
		 * @return {Object}
		 */
		getListSelection: function(id) {
			if(id == 'taxo') return this.__taxoListController.getSelection().getItem(0);
			else if(id == 'status') return this.__statusListController.getSelection().getItem(0);
			else if(id == 'author') return this.__usersListController.getSelection().getItem(0);
			else throw new Error('Unsupported id');
		},

		/**
		 * Return the view instance.
		 *
		 * @return {xima.ui.tabpages.ContentEditor}
		 */
		getView: function() {
			if(this.__view) return this.__view;

			this.__view = new xima.ui.tabpages.ContentEditor("New Content");
			return this.__view;
		}
	}
});
