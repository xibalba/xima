/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Taxo controller.
 * This controller implements the singleton pattern.
 * This controller take care of handle the interections of the user with the UI elements
 * of the taxonomy tab page view.
 */
qx.Class.define('xima.controller.Taxo', {
	extend: piche.controller.BaseAbstract,
	type : "singleton",

	construct: function() {
		this.base(arguments);

		var toolbar = this.getView().getToolBar();
		toolbar.getButton('new').addListener('execute', this.__handleNewTap, this);
		toolbar.getButton('edit').addListener('execute', this.__handleEditTap, this);

		this.getView().getWinEditor().getButton('save').addListener('execute', this.__handleSaveTap, this);
	},

	members: {
		__restResource: null,
		__view: null,

		/**
		 * Return the rest resource object.
		 * @return {xima.io.rest.Resource}
		 * @private
		 */
		__getRestResource: function() {
			if(this.__restResource) return this.__restResource;

			this.__restResource = new xima.io.rest.Resource({
				post: {method: 'POST', url: "taxo"},
				put: {method: 'PUT', url: "taxo/{id}" },
				get: {method: 'GET', url: "taxo" }
			});

			this.__restResource.addListener('getSuccess', this.__handleGetSuccess, this);
			this.__restResource.addListener('putSuccess', this.__handlePutSuccess, this);
			this.__restResource.addListener('postSuccess', this.__handlePostSuccess, this);

			return this.__restResource;
		},

		/**
		 * Handle the logic to execute when a user tap the toolbar new button.
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleNewTap: function(e) {
			var win = this.getView().getWinEditor();
			win.setMode(xima.ui.window.Editor.NEW_MODE);
			win.getForm().reset();
			win.open();
		},

		/**
		 * Handle the logic to execute when a user tap the toolbar edit save button.
		 * This method take care of take the table rowid, the row data a set it to the
		 * window editor and show it.
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleEditTap: function(e) {
			var table = this.getView().getTable();
			var rowIdx = table.getFocusedRow();

			if(qx.lang.Type.isNumber(rowIdx)) {
				var win = this.getView().getWinEditor();
				var formItems = win.getForm().getItems();
				var selectedData = table.getTableModel().getRowDataAsMap(rowIdx);

				for(var a in formItems) formItems[a].setValue(selectedData[a]);

				win.setMode(xima.ui.window.Editor.EDIT_MODE);
				win.open();
			}
		},

		/**
		 * Handle the logic to execute when a user tap the window editor save button.
		 * This mathod take care of execute a POST or PUT request by function of the editor mode.
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleSaveTap: function(e) {
			var rest = this.__getRestResource();

			var formItems = this.getView().getWinEditor().getForm().getItems();

			var table = this.getView().getTable();
			var rowIdx = table.getFocusedRow();
			var selectedData = table.getTableModel().getRowDataAsMap(rowIdx);

			var data = {};
			for(var i in formItems) data[i] = formItems[i].getValue();

			var win = this.getView().getWinEditor();
			win.block();

			if(win.getMode() == xima.ui.window.Editor.EDIT_MODE) {
				rest.put({ id: selectedData['id'] }, {
					"taxo": qx.util.Serializer.toJson(data)
				});
			}
			else {
				rest.post(null, {
					"taxo": qx.util.Serializer.toJson(data)
				});
			}
		},

		/**
		 * Handle the process for a successful GET request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handleGetSuccess: function(restEvent) {
			var response = restEvent.getRequest().getResponse();
			this.getView().getTable().getTableModel().setDataAsMapArray(response['taxo']);
		},

		/**
		 * Handle the process for a successful POST request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handlePostSuccess: function(restEvent){
			var win = this.getView().getWinEditor();
			var response = restEvent.getRequest().getResponse();

			if(response['success']) {
				this.getView().getTable().getTableModel().addRowsAsMapArray([response['taxo']]);
				win.unblock();
				win.close();
			} else throw new Error('Unhanled POST error.');
		},

		/**
		 * Handle the process for a successful  PUT request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handlePutSuccess: function(restEvent) {
			var win = this.getView().getWinEditor();
			var form = win.getForm();
			var formItems = form.getItems();
			var table = this.getView().getTable();

			var data = {};
			for(var i in formItems) data[i] = formItems[i].getValue();

			table.getTableModel().setValuesById(data, table.getFocusedRow());

			win.unblock();
			win.close();
		},

		/**
		 * Return the view instance.
		 * @return {xima.ui.tabpages.Taxo}
		 */
		getView: function() {
			if(this.__view) return this.__view;

			this.__view = new xima.ui.tabpages.Taxo();
			return this.__view;
		},

		dispatch: function() {
			this.getView().getTable().getTableModel().setData([]);
			this.__getRestResource().get();
		}
	}
});