/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 - 2017 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/

/**
 * Settings controller.
 * This controller implements the singleton pattern.
 * This controller take care of handle the interactions of the user with the UI elements
 * of the application settigs tab.
 */
qx.Class.define('xima.controller.Settings', {
	extend: piche.controller.BaseAbstract,
	type : "singleton",

	construct: function() {
		this.base(arguments);

		/*var toolbar = this.getView().getToolBar();
		toolbar.getButton('new').addListener('execute', this.__handleNewTap, this);
		toolbar.getButton('edit').addListener('execute', this.__handleEditTap, this);

		this.getView().getWinEditor().getButton('save').addListener('execute', this.__handleSaveTap, this);*/

		this.dispatch();
	},

	members: {
		__restResource: null,
		__userStore: null,
		__view: null,

		dispatch: function() {
			this.getView().getTab('roles')
				.addListener('appear', this.__composeRolesView, this);

			var userTab = this.getView().getTab('users');


			userTab.addListener('appear', this.__composeUsersView, this);

			userTab.getTable().getSelectionModel().addListener('changeSelection', this.__handleUserSelection, this);

			userTab.getToolBar().getButton('new')
				.addListener('execute', this.__handleUserNewBtnTap, this);

			userTab.getToolBar().getButton('edit')
				.addListener('execute', this.__handleUserEditBtnTap, this);

			userTab.getWinEditor().getToolBar().getButton('save')
				.addListener('execute', this.__handleUserEditorBtnSaveTap, this);
		},

		/**
		 *
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__composeRolesView: function(e) {
			var rolesView = e.getTarget();

			if(!rolesView.hasChildren()) {
				rolesView.compose();

				var rolesToolbar = rolesView.getToolBar('roles');
				var permissionsTolbar = rolesView.getToolBar('permissions');

				rolesToolbar.getButton('edit').addListener('execute', this.__handleRoleEditBtnTap, this);
				rolesToolbar.getButton('new').addListener('execute', this.__handleRoleNewBtnTap, this);

				permissionsTolbar.getButton('new').addListener('execute', function() {
					console.info('Nuevo permiso');
				}, this);

				permissionsTolbar.getButton('edit').addListener('execute', function() {
					console.info('Editar permiso');
				}, this);

				rolesView.getWinEditor('roles').getButton('save')
					.addListener('execute', this.__handleRolesBtnSaveTap, this);

				rolesView.getTable('roles').getSelectionModel()
					.addListener('changeSelection', this.__handleRoleSelectionChange, this);

				this.__getRestResource().getRoles();
				this.__getRestResource().getResources();
			}
		},

		__composeUsersView: function(e) {
			var usersView = e.getTarget();

			if(!usersView.hasChildren()) {
				usersView.compose();

				this.__userStore = new qx.data.store.Rest(this.__getRestResource(), "getUsers", {
					manipulateData: function(data) {
						var users = data['users'];

						for(var i in users) {
							users[i]['fullname'] = users[i]['person']['name'] + ' ' + users[i]['person']['surname'];
						}

						usersView.getTable().getTableModel().setDataAsMapArray(users);

						return data;
					}
				});

				this.__getRestResource().getUsers();
			}
		},

		__handleUserSelection: function(e) {
			if(e.getTarget().getSelectedCount() > 0) {
				var table = this.getView().getTab('users').getTable();
				var rowIdx = table.getFocusedRow();
				var data = table.getTableModel().getRowDataAsMap(rowIdx);
				this.__getRestResource().getUserRoles(null, {username: data['username']});
			}
		},

		/**1
		 *
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleRoleNewBtnTap: function(e) {
			var win = this.getView().getTab('roles').getWinEditor('roles');
			win.setMode(xima.ui.window.Editor.NEW_MODE);
			win.getForm().reset();
			win.open();
		},
		
		__handlePermissionNewBtnTap: function(e) {

		},

		/**
		 * Handle the logic to execute when a user tap the toolbar edit button.
		 * This method take care of take the table rowid, the row data a set it to the
		 * window editor and show it.
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleRoleEditBtnTap: function(e) {
			var table = this.getView().getTab('roles').getTable('roles');
			var rowIdx = table.getFocusedRow();

			if(qx.lang.Type.isNumber(rowIdx)) {
				var win = this.getView().getTab('roles').getWinEditor('roles');
				var formItems = win.getForm().getItems();
				var selectedData = table.getTableModel().getRowDataAsMap(rowIdx);

				for(var a in formItems) formItems[a].setValue(selectedData[a]);

				win.setMode(xima.ui.window.Editor.EDIT_MODE);
				win.open();
			}
		},

		__handleUserNewBtnTap: function(e) {
			var win = this.getView().getTab('users').getWinEditor();
			win.open();
			win.getBox('password').show();
		},

		__handleUserEditBtnTap: function(e) {
			var table = this.getView().getTab('users').getTable();
			var rowIdx = table.getFocusedRow();

			if(qx.lang.Type.isNumber(rowIdx)) {
				var selectedData = table.getTableModel().getRowDataAsMap(rowIdx);
				console.info(rowIdx);
				console.info(this.__userStore.getModel().getUsers().toArray());

				var model = this.__userStore.getModel().getUsers().filter(function(el) {
					return el.get('username') === selectedData['username']
				}).toArray()[0];

				var win = this.getView().getTab('users').getWinEditor();
				win.getBox('password').exclude();

				var userItems = win.getForm('user').getItems();
				var personItems = win.getForm('person').getItems();

				var a = null;

				for(a in userItems) userItems[a].setValue(model.get(a));
				for(a in personItems) personItems[a].setValue(model.get('person').get(a));

				win.open();

				console.info(model);
				console.info(model.get('username'));
				console.info(model.get('person').get('name'));
				//console.info(model.toArray()[0]);
			}
		},

		/**
		 *
		 * @param e
		 * @private
		 */
		__handleUserEditorBtnSaveTap: function(e) {
			var winEditor = this.getView().getTab('users').getWinEditor();
			var userForm = winEditor.getForm('user');
			var personForm = winEditor.getForm('person');
			var passwordBox = winEditor.getBox('password');
			var passwordForm = winEditor.getForm('password');

			if(userForm.validate() && personForm.validate()) {
				// Prepare user data
				var userData = {
					username: userForm.getItem('username').getValue(),
					email: userForm.getItem('email').getValue()
				};

				if(passwordBox.isVisible() && passwordForm.validate()) {
					// validate Password
					if(passwordForm.getItem('prepassword').getValue() !== passwordForm.getItem('newpassword').getValue()) {
						piche.ui.Dialog.error('Password fields does not match.');
					}

					var hasher = new jsSHA(userForm.getItem('username').getValue() + passwordForm.getItem('newpassword').getValue(), "TEXT");
					userData['password'] = hasher.getHash("SHA-256", "HEX");
				}

				// Prepare personal data
				var personData = {
					// did: personForm.getItem('did').getValue(),
					name: personForm.getItem('name').getValue(),
					surname: personForm.getItem('surname').getValue()
				};

				this.getView().block();

				if(passwordBox.isVisible()) {
					this.__getRestResource().postUser(null, {
						"user": qx.util.Serializer.toJson(userData),
						"person": qx.util.Serializer.toJson(personData)
					});
				}
				else {
					this.__getRestResource().putUser({id: userData['username']}, {
						"user": qx.util.Serializer.toJson(userData),
						"person": qx.util.Serializer.toJson(personData)
					});
				}

			}
		},

		/**
		 * Handle the logic to execute when a user tap the role window editor save button.
		 * This mathod take care of execute a POST or PUT request by function of the editor mode.
		 * @param e {qx.event.type.Event}
		 * @private
		 */
		__handleRolesBtnSaveTap: function(e) {
			var rest = this.__getRestResource();
			var win = this.getView().getTab('roles').getWinEditor('roles');
			var formItems = win.getForm().getItems();

			var table = this.getView().getTab('roles').getTable('roles');
			var rowIdx = table.getFocusedRow();
			var selectedData = table.getTableModel().getRowDataAsMap(rowIdx);

			var data = {};
			for(var i in formItems) data[i] = formItems[i].getValue();

			win.block();

			if(win.getMode() == xima.ui.window.Editor.EDIT_MODE) {
				rest.putRole({ id: selectedData['id'] }, {
					"role": qx.util.Serializer.toJson(data)
				});
			}
			else {
				rest.postRole(null, {
					"role": qx.util.Serializer.toJson(data)
				});
			}
		},

		__handleRoleSelectionChange: function(e) {
			var table = this.getView().getTab('roles').getTable('roles');
			var rowIdx = table.getFocusedRow();
			var id = table.getTableModel().getValue(0, rowIdx);

			console.info(id);

			/*this.__changeStatus(this.getView().getTable().getTableModel().getValue(3, rowIdx));
			if(id !== null) {
				this._getRestResource().invoke('getOperacionesFiltradas', null, {recurso_id: id});
				this.getView().block();
			}*/
		},

		/**
		 * Return the rest resource object.
		 * @return {xima.io.rest.Resource}
		 * @private
		 */
		__getRestResource: function() {
			if(this.__restResource) return this.__restResource;

			this.__restResource = new xima.io.rest.Resource({
				getRoles: {method: 'GET', url: "config/roles"},
				getUsers: {method: 'GET', url: "users"},

				putRole: {method: 'PUT', url: "config/roles/{id}" },
				postRole: {method: 'POST', url: "config/roles" },

				getResources: {method: 'GET', url: "config/resources"},

				getUserRoles: {method: 'GET', url: "user/roles"},

				postUser: {method: 'POST', url: "users"},
				putUser: {method: 'PUT', url: "users/{id}"}
			});

			this.__restResource.addListener('postUserSuccess', this.__handlePostUserSuccess, this);
			this.__restResource.addListener('putUserSuccess', this.__handlePostUserSuccess, this);

			this.__restResource.addListener('getRolesSuccess', this.__handleGetRolesSuccess, this);
			// this.__restResource.addListener('getUsersSuccess', this.__handleGetUsersSuccess, this);

			this.__restResource.addListener('getUserRolesSuccess', this.__handleGetUserRolesSuccess, this);

			this.__restResource.addListener('putRoleSuccess', this.__handlePutRoelsSuccess, this);
			this.__restResource.addListener('postRoleSuccess', this.__handlePostRoleSuccess, this);

			this.__restResource.addListener('getResourcesSuccess', this.__handleGetResourcesSuccess, this);

			return this.__restResource;
		},

		/**
		 * Handle the process for a successful GET request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handleGetRolesSuccess: function(restEvent) {
			var response = restEvent.getRequest().getResponse();
			this.getView().getTab('roles').getTable('roles').getTableModel().setDataAsMapArray(response['roles']);
		},

		/**
		 * Handle the process for a successful GET request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handleGetUsersSuccess: function(restEvent) {
			var response = restEvent.getRequest().getResponse();

			var data = response['users'];

			for(var i in data) {
				data[i]['fullname'] = data[i]['name'] + ' ' + data[i]['surname'];
			}

			this.getView().unblock();
			this.getView().getTab('users').getTable().getTableModel().setDataAsMapArray(data);
		},

		__handleGetUserRolesSuccess: function(restEvent) {
			var response = restEvent.getRequest().getResponse();
			this.getView().getTab('users').getRolesList().setListItems(response['roles'], '<b>{{label}}</b>', 'id');
		},

		/**
		 * Handle the process for a successful PUT request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handlePutRoelsSuccess: function(restEvent) {
			var win = this.getView().getTab('roles').getWinEditor('roles');
			var form = win.getForm();
			var formItems = form.getItems();
			var table = this.getView().getTab('roles').getTable('roles');

			var data = {};
			for(var i in formItems) data[i] = formItems[i].getValue();

			table.getTableModel().setValuesById(data, table.getFocusedRow());

			win.unblock();
			win.close();
		},

		/**
		 * Handle the process for a successful POST request.
		 * @param restEvent {qx.event.type.Rest}
		 * @private
		 */
		__handlePostRoleSuccess: function(restEvent) {
			var win = this.getView().getTab('roles').getWinEditor('roles');
			var response = restEvent.getRequest().getResponse();

			if(response['success']) {
				this.getView().getTab('roles').getTable('roles').getTableModel().addRowsAsMapArray([response['role']]);
				win.unblock();
				win.close();
			} else throw new Error('Unhanled POST error.');
		},

		__handlePostUserSuccess: function(restEvent) {
			this.getView().unblock();
			this.getView().getTab('users').getWinEditor().close();
			this.__getRestResource().getUsers();
		},

		__handleGetResourcesSuccess: function(restEvent) {
			var response = restEvent.getRequest().getResponse();

			if(response['success']) {
				console.info(response['resources']);
			}
		},

		/**
		 * Return the view instance.
		 * @return {xima.ui.tabpages.Settings}
		 */
		getView: function() {
			if(this.__view) return this.__view;

			this.__view = new xima.ui.tabpages.Settings();
			return this.__view;
		}
	}
});
