/* ****************************************************************

 @link https://gitlab.com/xibalba/xima
 @copyright 2016 Xibalba Lab
 @license MIT: http://opensource.org/licenses/MIT

 -- Xima: A web appication manager for Mezcal CMS --

 @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭

 *******************************************************************/


/**
 * This is the main application class of web application manager for Mezcal, "Xima".
 *
 * @asset(xima/*)
 */
qx.Class.define("xima.Application", {
	extend: qx.application.Standalone,

	members: {
		__frontController: null,
		__guiContainer: null,
		__header: null,
		__hashes: null,
		__apiUrl: null,

		/**
		 * This method contains the initial application code and gets called
		 * during startup of the application
		 *
		 * @lint ignoreDeprecated(alert)
		 */
		main: function() {
			this.base(arguments);

			this.getRoot().setBlockerColor('black');
			this.getRoot().setBlockerOpacity(0.2);
			this.getRoot().add(this.__getGuiContainer(), {edge: 0});
		},

		/**
		 * Return the Composite instance container for wrapps all application user interface,
		 * if there is no one, then create one and return it.
		 *
		 * @returns {qx.ui.container.Composite}
		 * @private
		 */
		__getGuiContainer: function() {
			if(this.__guiContainer) return this.__guiContainer;

			this.__guiContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox());

			this.__guiContainer.add(this.getHeader());
			this.__guiContainer.add(this.getFrontController().getView(), {flex: 1});

			this.__guiContainer.addListenerOnce('appear', function(e) {
				this.getFrontController().dispatch();
			}, this);

			return this.__guiContainer;
		},

		/**
		 * Return the UI header instance or create one and return it if don't exist.
		 * @returns {xima.ui.Header}
		 */
		getHeader: function() {
			if(this.__header) return this.__header;

			this.__header = new xima.ui.Header();
			return this.__header;
		},

		/**
		 * Shorthand for `xima.controller.Front.getInstance()`.
		 * @returns {xima.controller.Front}
		 */
		getFrontController: function() {
			return xima.controller.Front.getInstance();
		},

		/**
		 * Return the application name.
		 * @returns {string}
		 */
		getName: function() {
			return 'xima';
		},

		/**
		 * Return the application version.
		 * @returns {string}
		 */
		getVersion: function() {
			return 'Dev 0.1';
		},

		/**
		 * @todo
		 * @param id
		 * @returns {string}
		 */
		getHash: function(id) {
			id = id || 'app';

			if(!this.__hashes) this.__hashes = {};
			if(this.__hashes[id]) return this.__hashes;

			if(id == 'app') this.__hashes[id] = '@todoAppHash';
			else if(id == 'mz') this.__hashes[id] = '@todoMzHash';
			else throw new Error('Invalid di for hash');

			return this.__hashes[id];
		},

		/**
		 * Return the REST API string URL.
		 * @returns {string}
		 */
		getApiUrl: function() {
			if(this.__apiUrl) return this.__apiUrl;

			this.__apiUrl = document.location.origin + '/api/rest/';
			return this.__apiUrl;
		}
	}
});
