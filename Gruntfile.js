// requires
var util = require('util');
var qx = require("qooxdoo-sdk/5.0.1/tool/grunt");

// grunt
module.exports = function(grunt) {
  var config = {

    generator_config: {
      let: {
      }
    },

    common: {
      "APPLICATION" : "xima",
      "QOOXDOO_PATH" : "qooxdoo-sdk/5.0.1",
      "LOCALES": ["en"],
      "QXTHEME": "xima.theme.Theme"
    }

    /*
    myTask: {
      options: {},
      myTarget: {
        options: {}
      }
    }
    */
  };

  var mergedConf = qx.config.mergeConfig(config);
  // console.log(util.inspect(mergedConf, false, null));
  grunt.initConfig(mergedConf);

  qx.task.registerTasks(grunt);

  // grunt.loadNpmTasks('grunt-my-plugin');
};
